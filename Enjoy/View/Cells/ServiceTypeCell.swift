//
//  ServiceTypeCell.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/29/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import DropDown
import Kingfisher

class ServiceTypeCell: UICollectionViewCell {
    
    @IBOutlet weak var serviceImage: UIImageView!
    @IBOutlet weak var serviceName: UILabel!
    
    override var isSelected: Bool {
           didSet{
               if self.isSelected
               {
                self.borderColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
                self.serviceName.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)

               }
               else
               {
                self.borderColor = #colorLiteral(red: 0.6959015727, green: 0.6889173388, blue: 0.688760221, alpha: 1)
                self.serviceName.textColor = #colorLiteral(red: 0.2226176858, green: 0.2226238251, blue: 0.222620517, alpha: 1)
               }
           }
       }
    
    func setupCell(service: Category) {
        self.serviceImage.kf.setImage(with: URL(string: service.image?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""),options: [.scaleFactor(UIScreen.main.scale),.transition(.fade(1)),.cacheOriginalImage])
        self.serviceName.text = service.name
    }
}

class ArtistMenuCell: DropDownCell {
   @IBOutlet weak var artistImage: UIImageView!
   @IBOutlet weak var artistName: UILabel!
}
