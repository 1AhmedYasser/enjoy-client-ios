//
//  VideoCell.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/17/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
class VideoCell: UICollectionViewCell {
    
    @IBOutlet weak var videoCategory: DesignableButton!
    @IBOutlet weak var videoLikes: UILabel!
    @IBOutlet weak var videoViews: UILabel!
    @IBOutlet weak var videoTitle: UILabel!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var likesButton: UIButton!
    @IBOutlet weak var artistImage: DesignableImageView!
    @IBOutlet weak var likesImage: UIImageView!
    
    @IBOutlet weak var videoPlayer: UIView!
    @IBOutlet weak var stateButton: UIButton!
    @IBOutlet weak var thumbnail: UIImageView!
    
    var delegate: VideoCellDelegate?
    var player: AVPlayer?
    var playerLayer:AVPlayerLayer?
    var video: VideosData?
    var isLiked: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupVideo(video: VideosData) {
        
        videoLikes.text = "\(video.likes)"
        videoViews.text = "\(video.views)"
        videoTitle.text = "\(video.title)"
        artistName.text = "\(video.artist.name)"
        videoCategory.setTitle(video.category.name, for: .normal)
        (video.liked) ? (likesImage.image =  #imageLiteral(resourceName: "thumbs_up_filled")) : (likesImage.image =  #imageLiteral(resourceName: "thumbsUp"))
        isLiked = video.liked
        setEnjoyImage(thumbnail,video.thumb_nail ?? "")
        setEnjoyImage(artistImage,video.artist.image ?? "")
        self.video = video
        
        // load Video
        
        if player == nil {
            print("Added New")
            if video.video?.isEmpty == false {
                if let videoURL = URL(string: video.video!) {
                    player = AVPlayer(url: videoURL)
                    playerLayer = AVPlayerLayer(player: player)
                    playerLayer!.frame = CGRect(x: self.videoPlayer.frame.origin.x, y: self.videoPlayer.frame.origin.y, width: self.frame.width, height: self.videoPlayer.frame.height)
                    playerLayer!.videoGravity = AVLayerVideoGravity.resize
                    self.videoPlayer.layer.insertSublayer(playerLayer!, at: 0)
                }
            }
        } else {
            print("Added Before")
            if video.video?.isEmpty == false {
                if let videoURL = URL(string: video.video!) {
                    player = AVPlayer(url: videoURL)
                    playerLayer = AVPlayerLayer(player: player)
                    playerLayer!.frame = CGRect(x: self.videoPlayer.frame.origin.x, y: self.videoPlayer.frame.origin.y, width: self.frame.width, height: self.videoPlayer.frame.height)
                    playerLayer!.videoGravity = AVLayerVideoGravity.resize
                    self.videoPlayer.layer.sublayers?.remove(at: 0)
                    self.videoPlayer.layer.insertSublayer(playerLayer!, at: 0)
                }
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
    }
    
    @objc func playerDidFinishPlaying() {
        stateButton.setImage(#imageLiteral(resourceName: "replay"), for: .normal)
    }
    
    @IBAction func changeStatus(_ sender: UIButton) {
        if let player = self.player {
            thumbnail.image = nil
            
            if sender.imageView?.image == UIImage(named: "replay") {
                sender.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
                self.player?.seek(to: CMTime.zero)
                self.player?.play()
            } else {
                if player.timeControlStatus == AVPlayer.TimeControlStatus.playing {
                    stateButton.setImage(#imageLiteral(resourceName: "play"), for: .normal)
                    player.pause()
                } else {
                    stateButton.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
                    player.play()
                }
            }
        }
    }
    
    @IBAction func handleVideoLikes(_ sender: UIButton) {
        delegate?.likeVideo(cell: self)
    }
    
    @IBAction func showVideoInFullScreen(_ sender: Any) {
        //delegate?.fullScreenPressed(cell: self)
        delegate?.openMediaDetails(cell: self)
    }
    
    func setEnjoyImage(_ imageView: UIImageView,_ url: String) {
        imageView.kf.setImage(with: URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? url),options: [.scaleFactor(UIScreen.main.scale),.transition(.fade(1)),.cacheOriginalImage])
    }
}

class CategoryCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            contentView.leftAnchor.constraint(equalTo: leftAnchor),
            contentView.rightAnchor.constraint(equalTo: rightAnchor),
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    func setup(_ categoryTitle: String) {
        categoryName.text = categoryTitle
    }
    
    override var isSelected: Bool{
        didSet{
            if self.isSelected
            {
                self.backgroundColor = #colorLiteral(red: 0.716593504, green: 0.3097704053, blue: 0.5101804137, alpha: 1)
            }
            else
            {
                self.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            }
        }
    }
}

class AdCell: UICollectionViewCell {
    
    @IBOutlet weak var adImage: UIImageView!
    
    func setupAd(image: String) {
        adImage.kf.setImage(with: URL(string: image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? image),options: [.scaleFactor(UIScreen.main.scale),.transition(.fade(1)),.cacheOriginalImage])
    }
}

protocol VideoCellDelegate: class {
    func fullScreenPressed(cell: VideoCell)
    func openMediaDetails(cell: VideoCell)
    func likeVideo(cell: VideoCell)
}
