//
//  NotificationCell.swift
//  Enjoy
//
//  Created by Ahmed yasser on 1/16/20.
//  Copyright © 2020 Ahmed yasser. All rights reserved.
//

import UIKit

class NotificationCell: UICollectionViewCell {
    
    @IBOutlet weak var notificationTitle: UILabel!
    @IBOutlet weak var notificationMessage: UILabel!
    @IBOutlet weak var notificationLetter: UILabel!
    @IBOutlet weak var time: UILabel!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupCell(notification: NotificationsData) {
        notificationTitle.text = notification.title
        notificationMessage.text = notification.body
        notificationLetter.text = notification.title[0]
        time.text = notification.created_at
    }
}
