//
//  OrderStateCell.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/30/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import Kingfisher

class OrderStateCell: UICollectionViewCell {
    
    @IBOutlet weak var stateName: UILabel!
    @IBOutlet weak var selectionIndicator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            contentView.leftAnchor.constraint(equalTo: leftAnchor),
            contentView.rightAnchor.constraint(equalTo: rightAnchor),
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    override var isSelected: Bool{
        didSet{
            if self.isSelected
            {
                self.stateName.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                selectionIndicator.backgroundColor = #colorLiteral(red: 0.716593504, green: 0.3097704053, blue: 0.5101804137, alpha: 1)
            }
            else
            {
                self.stateName.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
                selectionIndicator.backgroundColor = UIColor.clear
            }
        }
    }
}

class StateCell: UICollectionViewCell {
    
    @IBOutlet weak var serviceImage: UIImageView!
    @IBOutlet weak var serviceTitle: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var stateIndicator: UIView!
    @IBOutlet weak var stateName: UILabel!
    @IBOutlet weak var optionsStack: UIStackView!
    @IBOutlet weak var viewVideo: UIImageView!
    
    var delegate: OrderStateDelegate?
    var order: OrderStateData?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setupCell(order: OrderStateData) {
        self.order = order
        serviceTitle.text = order.video_title
        time.text = order.created_at
        stateName.text = order.client_status.localized
        serviceImage.kf.setImage(with: URL(string: order.category_image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""),options: [.scaleFactor(UIScreen.main.scale),.transition(.fade(1)),.cacheOriginalImage])
        
        switch order.client_status {
        case "Accepted":
            handleCell(#colorLiteral(red: 0.2666666667, green: 0.537254902, blue: 1, alpha: 1))
        case "Done":
            handleCell(#colorLiteral(red: 0.2804551423, green: 0.740911305, blue: 0.1311946511, alpha: 1),true)
        case "Canceled":
            handleCell(#colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1))
        case "Rejected":
            handleCell(#colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1))
        case "Pending":
            handleCell(#colorLiteral(red: 1, green: 0.7489039302, blue: 0, alpha: 1))
        case "In Progress":
            handleCell(#colorLiteral(red: 0.8235294118, green: 0.1921568627, blue: 0.5098039216, alpha: 1))
        default:
            handleCell(#colorLiteral(red: 1, green: 0.7489039302, blue: 0, alpha: 1))
        }
    }
    
    func handleCell(_ color: UIColor,_ showOptions: Bool = false) {
        stateName.textColor = color
        stateIndicator.backgroundColor = color
        (showOptions) ? (optionsStack.isHidden = false) : (optionsStack.isHidden = true)
    }
    
    @IBAction func shareVideo(_ sender: UIButton) {
        delegate?.shareVideo(cell: self)
    }
    
    @IBAction func downloadVideo(_ sender: UIButton) {
        delegate?.DownloadVideo(cell: self)
    }
    
    @IBAction func viewVideo(_ sender: UIButton) {
        delegate?.viewVideo(cell: self)
    }
}

protocol OrderStateDelegate: class {
    func shareVideo(cell: StateCell)
    func viewVideo(cell: StateCell)
    func DownloadVideo(cell: StateCell)
}
