//
//  CountryCell.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/11/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit

class CountryCell: UITableViewCell {

    @IBOutlet weak var countryImage: UIImageView!
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var countryCode: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupCell (country : CountriesData) {
      countryImage!.kf.setImage(with: URL(string: country.image),options: [.scaleFactor(UIScreen.main.scale),.transition(.fade(1)),.cacheOriginalImage])
      countryName.text = country.name
      countryCode.text = country.code
    }
}
