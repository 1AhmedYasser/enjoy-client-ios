//
//  PromoCell.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/16/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import AVKit
import ScalingCarousel

class PromoCell: ScalingCarouselCell {
    
    @IBOutlet weak var videoView: UIView!
    
    var player: AVPlayer?
    
    let controlsView: UIView = {
        let controls = UIView()
        controls.backgroundColor = UIColor(white: 0, alpha: 0)
        return controls
    }()
    
    let pauseButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "playSlider"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        pauseButton.addTarget(self, action: #selector(changeStatus), for: .touchUpInside)
    }
    
    @objc func changeStatus() {
        if let player = self.player {
            
            if pauseButton.imageView?.image == UIImage(named: "SliderReplay") {
                pauseButton.setImage(#imageLiteral(resourceName: "pauseSlider"), for: .normal)
                self.player?.seek(to: CMTime.zero)
                self.player?.play()
            } else {
                if player.timeControlStatus == AVPlayer.TimeControlStatus.playing {
                    pauseButton.setImage(#imageLiteral(resourceName: "playSlider"), for: .normal)
                    player.pause()
                } else {
                    pauseButton.setImage(#imageLiteral(resourceName: "pauseSlider"), for: .normal)
                    player.play()
                }
            }
        }
    }
    
    func setupVideo(video: Slider) {
        // load Video
        mainView.layer.cornerRadius = 20
        videoView.layer.masksToBounds = true
        if video.video.isEmpty == false {
            if let videoURL = URL(string: video.video) {
                player = AVPlayer(url: videoURL)
                let playerLayer = AVPlayerLayer(player: player)
                playerLayer.frame = CGRect(x: 0, y: 0, width: 305, height: 185)
                playerLayer.videoGravity = AVLayerVideoGravity.resize
                videoView.layer.addSublayer(playerLayer)
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
        
        controlsView.layer.cornerRadius = 20
        controlsView.frame = CGRect(x: 0, y: 0, width: 305, height: 185)
        self.videoView.addSubview(controlsView)
        controlsView.addSubview(pauseButton)
        pauseButton.centerXAnchor.constraint(equalTo: videoView.centerXAnchor).isActive = true
        pauseButton.centerYAnchor.constraint(equalTo: videoView.centerYAnchor).isActive = true
        pauseButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        pauseButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    @objc func playerDidFinishPlaying() {
        pauseButton.setImage(#imageLiteral(resourceName: "SliderReplay"), for: .normal)
    }
}


class HomeHeader: UICollectionReusableView {
    
    @IBOutlet weak var promosCollectionView: ScalingCarouselView!
    @IBOutlet weak var sliderControl: UIPageControl!
    
    var sliderVideos = [Slider]()
}

extension HomeHeader: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let currentCenterIndex = promosCollectionView.currentCenterCellIndex?.row else { return }
        sliderControl.currentPage = currentCenterIndex
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sliderVideos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let promosCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PromosCell", for: indexPath) as! PromoCell
        promosCell.layer.cornerRadius = 20
        promosCell.setupVideo(video: sliderVideos[indexPath.row])
        DispatchQueue.main.async {
            promosCell.setNeedsLayout()
            promosCell.layoutIfNeeded()
        }
        return promosCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        promosCollectionView.scrollToItem(at: indexPath, at: .top, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
