//
//  ArtistCell.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/16/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import AVKit

class ArtistCell: UITableViewCell {
    
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var artistImage: DesignableImageView!
    @IBOutlet weak var worksOfArts: UILabel!
    @IBOutlet weak var price: UILabel!
    
    var delegate: ArtistCellDelegate?
    
    var artist: ArtistsData?
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        selected ? (contentView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)) : (contentView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
    }
    
    func setupCell (artist : ArtistsData) {
        artistImage!.kf.setImage(with: URL(string: artist.image?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? artist.image ?? ""),placeholder: UIImage(named: "Guest"),options: [.scaleFactor(UIScreen.main.scale),.transition(.fade(1)),.cacheOriginalImage])
        artistName.text = artist.name
        worksOfArts.text = "\(artist.orders_no)"
        price.text = "\(artist.price)"
        self.artist = artist
    }
    
    @IBAction func requestService(_ sender: DesignableButton) {
        delegate?.requestServicePressed?(cell: self)
    }
}

class ArtistPromoHeader: UICollectionReusableView {
    
    @IBOutlet weak var videoView: UIView!
    var player: AVPlayer?
    @IBOutlet weak var statusButton: UIButton!
    
    func setupVideo(video: String) {
        
        // load Video
        if video.isEmpty == false {
            if let videoURL = URL(string: video) {
                if player == nil {
                    player = AVPlayer(url: videoURL)
                    let playerLayer = AVPlayerLayer(player: player)
                    playerLayer.frame = CGRect(x: 0, y: 0, width: self.videoView.frame.width, height: self.videoView.frame.height)
                    videoView.layer.masksToBounds = true
                    playerLayer.cornerRadius = 10
                    playerLayer.videoGravity = AVLayerVideoGravity.resize
                    self.videoView.layer.insertSublayer(playerLayer, at: 0)
                }
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
    }
    
    @objc func playerDidFinishPlaying() {
        statusButton.setImage(#imageLiteral(resourceName: "SliderReplay"), for: .normal)
    }
    
    @IBAction func handlePromoState(_ sender: UIButton) {
        if let player = self.player {
            if sender.imageView?.image == UIImage(named: "SliderReplay") {
                sender.setImage(#imageLiteral(resourceName: "pauseSlider"), for: .normal)
                self.player?.seek(to: CMTime.zero)
                self.player?.play()
            } else {
                if player.timeControlStatus == AVPlayer.TimeControlStatus.playing {
                    sender.setImage(#imageLiteral(resourceName: "playSlider"), for: .normal)
                    player.pause()
                } else {
                    sender.setImage(#imageLiteral(resourceName: "pauseSlider"), for: .normal)
                    player.play()
                }
            }
        }
    }
}

class ArtistVideoCell: UICollectionViewCell {
    
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var videoTitle: UILabel!
    @IBOutlet weak var thumbnail: UIImageView!
    var player: AVPlayer?
    var delegate: ArtistCellDelegate?
    var playerLayer:AVPlayerLayer?
    @IBOutlet weak var statusButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupVideo(video: String,thumbnail: String,title: String) {
        
        // load Video
        if video.isEmpty == false {
            if player == nil {
                if let videoURL = URL(string: video) {
                    player = AVPlayer(url: videoURL)
                    playerLayer = AVPlayerLayer(player: player)
                    playerLayer!.frame = self.videoView.frame
                    videoView.layer.masksToBounds = true
                    playerLayer?.videoGravity = AVLayerVideoGravity.resize
                    self.videoView.layer.insertSublayer(playerLayer!, at: 0)
                }
            } else {
                if video.isEmpty == false {
                    if let videoURL = URL(string: video) {
                        player = AVPlayer(url: videoURL)
                        playerLayer = AVPlayerLayer(player: player)
                        playerLayer!.frame = self.videoView.frame
                        videoView.layer.masksToBounds = true
                        playerLayer!.videoGravity = AVLayerVideoGravity.resize
                        self.videoView.layer.sublayers?.remove(at: 0)
                        self.videoView.layer.insertSublayer(playerLayer!, at: 0)
                    }
                }
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
        
        self.thumbnail.kf.setImage(with: URL(string: thumbnail.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""),options: [.scaleFactor(UIScreen.main.scale),.transition(.fade(1)),.cacheOriginalImage])
        self.videoTitle.text = title
    }
    
    @objc func playerDidFinishPlaying() {
        statusButton.setImage(#imageLiteral(resourceName: "replay"), for: .normal)
    }
    
    @IBAction func handleVideoState(_ sender: UIButton) {
        if let player = self.player {
            thumbnail.image = nil
            if sender.imageView?.image == UIImage(named: "replay") {
                sender.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
                self.player?.seek(to: CMTime.zero)
                self.player?.play()
            } else {
                if player.timeControlStatus == AVPlayer.TimeControlStatus.playing {
                    sender.setImage(#imageLiteral(resourceName: "play"), for: .normal)
                    player.pause()
                } else {
                    sender.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
                    player.play()
                }
            }
        }
        
    }
    
    @IBAction func openVideoInFullScreen(_ sender: UIButton) {
        delegate?.fullScreenPressed?(cell: self)
    }
}

@objc protocol ArtistCellDelegate: class {
    @objc optional func fullScreenPressed(cell: ArtistVideoCell)
    @objc optional func requestServicePressed(cell: ArtistCell)
    
}
