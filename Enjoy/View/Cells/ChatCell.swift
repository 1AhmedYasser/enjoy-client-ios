//
//  ChatCell.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/24/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import MOLH

class ChatCell: UITableViewCell {

    @IBOutlet weak var userImage: DesignableImageView!
    @IBOutlet weak var messageHolder: DesignableView!
    @IBOutlet weak var messageStack: UIStackView!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var time: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setupUserMessage() {
       if MOLHLanguage.currentAppleLanguage() == "en" {
            self.messageHolder.MaskedCorners = "1,3,4"
        } else {
            self.messageHolder.MaskedCorners = "2,3,4"
        }
    }

    func setupSupportMessage() {
        self.messageStack.alignment = .trailing
        self.userImage.isHidden = true
        self.message.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.messageHolder.backgroundColor = #colorLiteral(red: 0.4004209638, green: 0.1157918349, blue: 0.4760776162, alpha: 1)
        if MOLHLanguage.currentAppleLanguage() == "en" {
            self.messageHolder.MaskedCorners = "2,3,4"
        } else {
            self.messageHolder.MaskedCorners = "1,3,4"
        }
    }
}
