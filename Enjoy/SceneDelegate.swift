//
//  SceneDelegate.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/10/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import MOLH
import Firebase
import DropDown
import IQKeyboardManager

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    
    @available(iOS 13.0, *)
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        Messaging.messaging().delegate = self
        
        if FirebaseApp.app() == nil {
            FirebaseApp.configure()
        }
        
        fetchShareLink()
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                UserDefaults.standard.set(result.token, forKey: Constants.fcmToken)
            }
        }
        
        Thread.sleep(forTimeInterval: 3.0)
        MOLH.shared.activate(true)
        DropDown.startListeningToKeyboard()
        loadEnjoyInfo()
        IQKeyboardManager.shared().isEnabled = true
        if (EnjoyData.shared.openedEnjoyBefore()) {
            rootVC(storyBoard: "Home", identifier: "HomeMainVC")
        } else {
            rootVC(storyBoard: "Main", identifier: "MainVC")
        }
        guard let _ = (scene as? UIWindowScene) else { return }
    }
    
    @available(iOS 13.0, *)
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        for urlContext in URLContexts {
            let url = urlContext.url
            Auth.auth().canHandle(url)
        }
    }
    
    @available(iOS 13.0, *)
    func sceneDidDisconnect(_ scene: UIScene) {
        if EnjoyData.shared.isUserLoggedIn() == false {
            UserDefaults.standard.set("", forKey: Constants.userToken)
            UserDefaults.standard.set("", forKey: Constants.userId)
            
        }
    }
    
    func fetchShareLink() {
        let remoteConfig = RemoteConfig.remoteConfig()
        let settings = RemoteConfigSettings()
        settings.minimumFetchInterval = 0
        remoteConfig.configSettings = settings
        
        remoteConfig.fetch(withExpirationDuration: TimeInterval(0)) { (status, error) -> Void in
            if status == .success {
                print("Config fetched!")
                remoteConfig.activate(completionHandler: { (error) in
                    let shareLink = remoteConfig["client_share_url"]
                    UserDefaults.standard.set(shareLink.stringValue, forKey: Constants.shareLink)
                })
            } else {
                print("Config not fetched")
                print("Error: \(error?.localizedDescription ?? "No error available.")")
            }
        }
    }
    
    func reset() {
        rootVC(storyBoard: "Home", identifier: "HomeMainVC")
    }
    
    func rootVC(storyBoard: String,identifier: String) {
        let initialViewController = UIStoryboard(name: storyBoard, bundle: nil).instantiateViewController(withIdentifier: identifier)
        self.window?.rootViewController = initialViewController
    }
    
    func loadEnjoyInfo() {
        EnjoyAPI.EnjoyRequest(NConstants.contactUs,clients: false,ContactUs.self,isHeaders: false) { (contactInfo, errorMessage, error) in
            if error != nil {
                print(error!.localizedDescription)
                return
            }
            
            if errorMessage.isEmpty == false {
                print(errorMessage)
            } else {
                print(contactInfo!.message)
                EnjoyData.shared.saveContactInfo(info: contactInfo!.data)
            }
        }
        
        EnjoyAPI.EnjoyRequest(NConstants.data,clients: false,DetailsData.self,isHeaders: false) { (details, errorMessage, error) in
            if error != nil {
                print(error!.localizedDescription)
                return
            }
            
            if errorMessage.isEmpty == false {
                print(errorMessage)
            } else {
                print(details!.message)
                EnjoyData.shared.saveDetails(data: details!.data)
            }
        }
    }
}


extension SceneDelegate: MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        UserDefaults.standard.set(fcmToken, forKey: Constants.fcmToken)
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Remote Message is \(remoteMessage.appData)")
    }
}
