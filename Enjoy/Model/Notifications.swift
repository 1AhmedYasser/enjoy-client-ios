//
//  Notification.swift
//  Enjoy
//
//  Created by Ahmed yasser on 1/16/20.
//  Copyright © 2020 Ahmed yasser. All rights reserved.
//

import Foundation

struct Notifications: Codable {
    let status_code: Int
    let success: Bool
    let data: [NotificationsData]
    let message: String
}

struct NotificationsData: Codable {
    let id: Int
    let body, title, created_at: String
}

struct NotificationsCount: Codable {
    let status_code: Int
    let success: Bool
    let data: Int
    let message: String
}
