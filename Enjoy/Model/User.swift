//
//  User.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/10/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import Foundation

struct User: Codable {
    let status_code: Int
    let success: Bool
    let data: UserData
    let message: String
}

struct UserData: Codable {
    let id: Int
    let username, email: String
    let country_id, city_id: Int?
    let phone_number: String
    let image: String?
    let valid: String
    let token: String?
    let country_text,city_text:String?
}

