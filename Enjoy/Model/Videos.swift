//
//  Videos.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/17/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import Foundation

struct Home: Codable {
    let status_code: Int
    let success: Bool
    let data: HomeData
    let message: String
}

struct HomeData: Codable {
    let slider: [Slider]
    let videos: HomeVideos
}

struct Slider: Codable {
    let video: String
}

struct HomeVideos: Codable {
    let videos: [VideosData]
    let next_page: String?
    let ad: Ad
}

struct Ad: Codable {
    let image: String
}

struct Videos: Codable {
    let status_code: Int
    let success: Bool
    let data: [VideosData]
    let message: String
}

struct VideosData: Codable {
    let id: Int
    let title: String
    let thumb_nail: String?
    let video: String?
    let views, likes: Int
    let liked: Bool
    let description: String
    let artist: Artist
    let category: Category
}

struct CategoryVideos: Codable {
    let status_code: Int
    let success: Bool
    let data: CategoryVideosData
    let message: String
}

struct CategoryVideosData: Codable {
    let videos: [VideosData]
}
