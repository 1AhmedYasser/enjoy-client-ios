//
//  Cities.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/11/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import Foundation

struct Cities: Codable {
    let status_code: Int
    let success: Bool
    let data: [CitiesData]
    let message: String
}

struct CitiesData: Codable {
    let id: Int
    let name: String
}
