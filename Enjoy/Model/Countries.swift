//
//  Countries.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/11/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import Foundation

struct Countries: Codable {
    let status_code: Int
    let success: Bool
    let data: [CountriesData]
    let message: String
}

struct CountriesData: Codable {
    let id: Int
    let code: String
    let image: String
    let name: String
}
