//
//  EnjoyData.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/15/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import Foundation

struct DetailsData: Codable {
    let status_code: Int
    let success: Bool
    let data: DetailsInnerData
    let message: String
}

struct DetailsInnerData: Codable {
    let about_app, terms_conditions, privacy_policy: String
}
