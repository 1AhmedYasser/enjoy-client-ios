//
//  Order.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/30/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import Foundation

struct OrderState: Codable {
    let status_code: Int
    let success: Bool
    let data: [OrderStateData]
    let message: String
}

struct OrderStateData: Codable {
    let id: Int
    let created_at, client_status, video_title, video_description: String
    let category_image: String
    let category_title, artist_title: String
    let video_cost: Int
    let artist_image: String
    let video_url: String?
    let video_thumb_nail: String?
}
