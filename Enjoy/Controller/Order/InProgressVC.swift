//
//  InProgressVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/30/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import MOLH
import Kingfisher

class InProgressVC: UIViewController {
    
    var order: OrderStateData?
    @IBOutlet weak var artistImage: UIImageView!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var serviceImage: UIImageView!
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var serviceDescription: UILabel!
    @IBOutlet weak var notificationIcon: BadgeBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        artistName.text = order?.artist_title
        serviceName.text = order?.category_title
        serviceDescription.text = order?.video_description
        artistImage.kf.setImage(with: URL(string: order?.artist_image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""),options: [.scaleFactor(UIScreen.main.scale),.transition(.fade(1)),.cacheOriginalImage])
        serviceImage.kf.setImage(with: URL(string: order?.category_image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""),options: [.scaleFactor(UIScreen.main.scale),.transition(.fade(1)),.cacheOriginalImage])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notificationIcon.badgeNumber = EnjoyData.shared.getIntValue(Constants.notificationCount)
        
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "RightArrowLanguage"), style: .plain, target: self, action: #selector(goBack(_:)))
            backButton.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.navigationItem.leftBarButtonItem  = backButton
        }
    }
}
