//
//  OrderEstimationVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/31/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import MOLH
import NVActivityIndicatorView

class OrderEstimationVC: UIViewController {
    
    var order: OrderStateData?
    var state: Bool? = true
    @IBOutlet weak var notificationIcon: BadgeBarButtonItem!
    
    @IBOutlet weak var stateText: UILabel!
    @IBOutlet weak var stateImage: UIImageView!
    @IBOutlet weak var stateGuidanceText: UILabel!
    @IBOutlet weak var rejectionTitle: UILabel!
    @IBOutlet weak var rejectionDescription: UILabel!
    @IBOutlet weak var processButton: UIButton!
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if state == false {
            stateImage.image = #imageLiteral(resourceName: "OrderRejected")
            stateText.text = "Your order is Rejected".localized
            stateText.textColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
            stateGuidanceText.text = "we are sorry !!!".localized
            rejectionTitle.isHidden = false
            rejectionDescription.isHidden = false
            processButton.setTitle("UPDATE ORDER".localized, for: .normal)
            processButton.backgroundColor = #colorLiteral(red: 0.8549019694, green: 0.250980407, blue: 0.4784313738, alpha: 1)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notificationIcon.badgeNumber = EnjoyData.shared.getIntValue(Constants.notificationCount)
        
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "RightArrowLanguage"), style: .plain, target: self, action: #selector(goBack(_:)))
            backButton.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.navigationItem.leftBarButtonItem  = backButton
        }
        
    }
    
    @IBAction func processOrder(_ sender: UIButton) {
        if state == false {
            self.openVC(storyBoard: "Order", identifier: "RequestServiceVC")
        } else {
            let paymentVC = self.storyboard!.instantiateViewController(withIdentifier: "OrderPaymentVC") as! OrderPaymentVC
            paymentVC.order = self.order
            self.navigationController!.pushViewController(paymentVC, animated: true)
        }
    }
    
    @IBAction func cancelOrder(_ sender: UIButton) {
        loadingIndicator.startAnimating()
        sender.isEnabled = false
        if let orderId = order?.id {
            let parameters = ["order_id":orderId]
            EnjoyAPI.EnjoyRequest(NConstants.cancelOrder,EnjoyError.self,parameters,isHeaders: true,.post) { (message, errorMessage, error) in
                self.loadingIndicator.stopAnimating()
                sender.isEnabled = true
                if error != nil {
                    self.enjoyToast(error!.localizedDescription)
                    return
                }
                
                if errorMessage.isEmpty == false {
                    self.enjoyToast(errorMessage)
                } else {
                    if message!.success == true {
                        self.enjoyToast(errorMessage)
                        self.goBack(sender)
                    } else {
                        self.enjoyToast(message!.message)
                    }
                }
            }
        }
    }
}
