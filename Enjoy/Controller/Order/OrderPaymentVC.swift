//
//  OrderPaymentVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/31/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import MOLH
import NVActivityIndicatorView

class OrderPaymentVC: UIViewController {
    
    var order: OrderStateData?
    
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    @IBOutlet weak var notificationIcon: BadgeBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notificationIcon.badgeNumber = EnjoyData.shared.getIntValue(Constants.notificationCount)
        
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "RightArrowLanguage"), style: .plain, target: self, action: #selector(goBack(_:)))
            backButton.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.navigationItem.leftBarButtonItem  = backButton
        }
        
    }
    
    @IBAction func pay(_ sender: UIButton) {
        loadingIndicator.startAnimating()
        sender.isEnabled = false
        if let orderId = order?.id {
            let parameters = ["order_status": "Accepted","order_id":orderId] as [String : Any]
            EnjoyAPI.EnjoyRequest(NConstants.payOrder,EnjoyError.self,parameters,isHeaders: true,.post) { (message, errorMessage, error) in
                self.loadingIndicator.stopAnimating()
                sender.isEnabled = true
                if error != nil {
                    self.enjoyToast(error!.localizedDescription)
                    return
                }
                
                if errorMessage.isEmpty == false {
                    self.enjoyToast(errorMessage)
                } else {
                    if message!.success == true {
                        self.enjoyToast(errorMessage)
                        self.openVC(storyBoard: "Order", identifier: "OrderComplete")
                    } else {
                        self.enjoyToast(message!.message)
                    }
                }
            }
        }
    }
}
