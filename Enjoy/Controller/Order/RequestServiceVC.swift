//
//  RequestServiceVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/29/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import DropDown
import Kingfisher
import GrowingTextView
import MOLH
import NVActivityIndicatorView

class RequestServiceVC: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var categories = [Category]()
    var artists = [ArtistsData]()
    @IBOutlet weak var artistsMenu: UIView!
    @IBOutlet weak var artistsMenuArrow: UIImageView!
    @IBOutlet weak var selectedArtistName: UILabel!
    @IBOutlet weak var selectedArtistImage: UIImageView!
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    
    var passedArtist: ArtistsData?
    var mediaArtistDetails:Artist?
    var selectedCategoryid:Int?
    var selectedArtistid:Int?
    
    let dropDown = DropDown()
    @IBOutlet weak var artistPrice: UILabel!
    @IBOutlet weak var ServiceTitle: UITextField!
    @IBOutlet weak var ServiceDescription: GrowingTextView!
    @IBOutlet weak var notificationIcon: BadgeBarButtonItem!
    
    var backButtonImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("passed Artist", passedArtist)
        print("mediaArtistDetails", mediaArtistDetails)
        
        if MOLHLanguage.currentAppleLanguage() == "en" {
            backButtonImage = #imageLiteral(resourceName: "LeftArrow")
        } else {
            backButtonImage = #imageLiteral(resourceName: "RightArrowLanguage")
        }
        
        if self.passedArtist != nil {
            self.selectedArtistName.text = passedArtist!.name
            self.selectedArtistImage.kf.setImage(with: URL(string: passedArtist!.image?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""),placeholder: UIImage(named: "Guest"),options: [.scaleFactor(UIScreen.main.scale),.transition(.fade(1)),.cacheOriginalImage])
            self.artistPrice.text = "\(passedArtist!.price) $"
            self.selectedArtistid = passedArtist?.id
            
            let leftBarButton = UIBarButtonItem(image: backButtonImage, style: .done, target: self, action: #selector(goBack(_:)))
            leftBarButton.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            self.navigationItem.leftBarButtonItem = leftBarButton
            applyEnjoyGradient(nav: self.navigationController!)
        }
        
        if self.mediaArtistDetails != nil {
            self.selectedArtistName.text = mediaArtistDetails!.name
            self.selectedArtistImage.kf.setImage(with: URL(string: mediaArtistDetails!.image?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""),placeholder: UIImage(named: "Guest"),options: [.scaleFactor(UIScreen.main.scale),.transition(.fade(1)),.cacheOriginalImage])
            self.artistPrice.text = "\(mediaArtistDetails!.price) $"
            self.selectedArtistid = mediaArtistDetails?.id
            
            let leftBarButton = UIBarButtonItem(image: backButtonImage, style: .done, target: self, action: #selector(goBack(_:)))
            leftBarButton.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            self.navigationItem.leftBarButtonItem = leftBarButton
            applyEnjoyGradient(nav: self.navigationController!)
        }
        
        loadServices()
        
        setupArtistsMenu()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notificationIcon.badgeNumber = EnjoyData.shared.getIntValue(Constants.notificationCount)
        ServiceDescription.placeholder = "Type your service description here ...".localized
        
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            ServiceDescription.textAlignment = .right
        } else {
            ServiceDescription.textAlignment = .left
        }
        
    }
    
    fileprivate func setupArtistsMenu() {
        dropDown.anchorView = artistsMenu
        
        dropDown.cancelAction = {self.artistsMenuArrow.image = #imageLiteral(resourceName: "downArrow")}
        dropDown.willShowAction = {self.artistsMenuArrow.image = #imageLiteral(resourceName: "up")}
        dropDown.cellNib = UINib(nibName: "Artist", bundle: nil)
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let artistCell = cell as? ArtistMenuCell else { return }
            artistCell.artistName.text = item
            artistCell.artistImage.kf.setImage(with: URL(string: self.artists[index].image?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""),placeholder: UIImage(named: "Guest"),options: [.scaleFactor(UIScreen.main.scale),.transition(.fade(1)),.cacheOriginalImage])
        }
        
        dropDown.selectionAction = {(index: Int, item: String) in
            self.selectedArtistName.text = item
            self.selectedArtistImage.kf.setImage(with: URL(string: self.artists[index].image?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""),placeholder: UIImage(named: "Guest"),options: [.scaleFactor(UIScreen.main.scale),.transition(.fade(1)),.cacheOriginalImage])
            self.artistsMenuArrow.image = #imageLiteral(resourceName: "downArrow")
            self.artistPrice.text = "\(self.artists[index].price) $"
            self.selectedArtistid = self.artists[index].id
            self.dropDown.hide()
        }
        
        LoadArtists()
        
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds  .height)!)
        artistsMenu.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleArtistsMenu)))
    }
    
    @objc func handleArtistsMenu() {
        dropDown.show()
    }
    
    
    
    fileprivate func loadServices() {
        loadingIndicator.startAnimating()
        EnjoyAPI.EnjoyRequest(NConstants.categories,clients: false, Categories.self,isHeaders: false) { (categories, errorMessage, error) in
            if error != nil {
                self.enjoyToast(error!.localizedDescription)
                return
            }
            
            if errorMessage.isEmpty == false {
                self.enjoyToast(errorMessage)
            } else {
                if categories!.success == true {
                    self.categories = categories!.data
                    self.collectionView.reloadData()
                } else {
                    self.enjoyToast(categories!.message)
                }
            }
        }
    }
    
    fileprivate func LoadArtists() {
        EnjoyAPI.EnjoyRequest(NConstants.artists,clients: false, Artists.self,isHeaders: false) { (artists, errorMessage, error) in
            self.loadingIndicator.stopAnimating()
            if error != nil {
                self.enjoyToast(error!.localizedDescription)
                return
            }
            
            if errorMessage.isEmpty == false {
                self.enjoyToast(errorMessage)
            } else {
                if artists!.success == true {
                    self.artists = artists!.data
                    for artist in self.artists {
                        self.dropDown.dataSource.append(artist.name)
                    }
                    if self.passedArtist == nil && self.mediaArtistDetails == nil {
                        self.selectedArtistName.text = self.artists[0].name
                        self.selectedArtistImage.kf.setImage(with: URL(string: self.artists[0].image?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""),placeholder: UIImage(named: "Guest"),options: [.scaleFactor(UIScreen.main.scale),.transition(.fade(1)),.cacheOriginalImage])
                        self.artistPrice.text = "\(self.artists[0].price) $"
                        self.selectedArtistid = self.artists[0].id
                    }
                } else {
                    self.enjoyToast(artists!.message)
                }
            }
        }
    }
    
    @IBAction func sendRequest(_ sender: UIButton) {
        
        if ServiceTitle.text!.isEmpty || ServiceDescription.text.isEmpty {
            enjoyToast("Please fill the empty fields".localized)
            return
        }
        
        if selectedArtistid == nil {
            enjoyToast("Please select an artist".localized)
            return
        }
        
        if selectedCategoryid == nil {
            enjoyToast("Please choose a service type".localized)
            return
        }
        sendServiceRequest(sender)
    }
    
    func sendServiceRequest(_ sender: UIButton) {
        loadingIndicator.startAnimating()
        sender.isEnabled = false
        let parameters = ["artist_id": selectedArtistid!,
                          "category_id": selectedCategoryid!,
                          "title" : ServiceTitle.text!,
                          "description": ServiceDescription.text!] as [String : Any]
        EnjoyAPI.EnjoyRequest(NConstants.placeOrder,EnjoyError.self,parameters,isHeaders: true,.post) { (message, errorMessage, error) in
            self.loadingIndicator.stopAnimating()
            sender.isEnabled = true
            if error != nil {
                self.enjoyToast(error!.localizedDescription)
                return
            }
            
            if errorMessage.isEmpty == false {
                self.enjoyToast(errorMessage)
            } else {
                if message!.success == true {
                    self.enjoyToast(message!.message)
                    self.openVC(storyBoard: "Order", identifier: "MyOrdersVC")
                } else {
                    self.enjoyToast(message!.message)
                }
            }
        }
    }
}

extension RequestServiceVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let serviceTypeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ServiceTypeCell", for: indexPath) as! ServiceTypeCell
        serviceTypeCell.setupCell(service: categories[indexPath.row])
        return serviceTypeCell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedCategoryid = categories[indexPath.row].id
    }
}
