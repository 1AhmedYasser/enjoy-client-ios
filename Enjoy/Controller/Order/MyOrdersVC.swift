//
//  MyOrdersVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/30/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import MBProgressHUD

class MyOrdersVC: UIViewController {
    
    @IBOutlet weak var statesCV: UICollectionView!
    @IBOutlet weak var OrderStatesCV: UICollectionView!
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    @IBOutlet weak var statesFlowlayout: UICollectionViewFlowLayout!
    @IBOutlet weak var notificationIcon: BadgeBarButtonItem!
    
    let states: [String] = ["All","Accepted","In Progress","Pending","Completed","Rejected","Canceled"]
    var currentState: String = "All"
    var orders = [OrderStateData]()
    var progress: Float = 0.0
    var task: URLSessionTask?
    
    lazy var session : URLSession = {
        return URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: OperationQueue.main)
    }()
    
    var selectedState = [OrderStateData]() {
        didSet {
            
            UIView.transition(with: self.OrderStatesCV, duration: 0.5, options: .transitionCrossDissolve, animations: {
                self.OrderStatesCV.reloadData()
            }, completion: nil)
        }
    }
    
    var stateSelected = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        applyEnjoyGradient(nav: self.navigationController!,collectionView: statesCV)
        statesCV.selectItem(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .top)
        
        statesFlowlayout.itemSize = UICollectionViewFlowLayout.automaticSize
        statesFlowlayout.estimatedItemSize = CGSize(width: 76, height: 50)
        
        //LoadOrders()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        notificationIcon.badgeNumber = EnjoyData.shared.getIntValue(Constants.notificationCount)
        
        LoadOrders()
        if self.navigationController!.navigationBar.isTranslucent {
            applyEnjoyGradient(nav: self.navigationController!)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.task?.cancel()
    }
    
    func showEmptyResultMessage(show: Bool,status: String) {
        if show {
            OrderStatesCV.emptyDataSetView { (view) in
                view.image(UIImage(named: "NoOrders")).detailLabelString(NSAttributedString(string: "No Orders".localized)).shouldFadeIn(true).verticalSpace(37.2)
            }
        } else {
            OrderStatesCV.emptyDataSetView { (view) in
                view.detailLabelString(NSAttributedString(string: ""))
                
            }
        }
    }
    
    @objc func cancelVideoDownload() {
        task?.cancel()
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func downLoadEnjoyVideo(url:String) {
        let hud =  MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.mode = .annularDeterminate
        hud.label.text = "Downloading"
        hud.detailsLabel.text = "Cancel"
        let tap = UITapGestureRecognizer(target: self, action: #selector(cancelVideoDownload))
        hud.addGestureRecognizer(tap)
        DispatchQueue.global(qos: .default).async(execute: {() -> Void in
            while self.progress < 1.0 {
                DispatchQueue.main.async(execute: {() -> Void in
                    MBProgressHUD(for: self.view)?.progress = self.progress
                })
                usleep(50000)
            }
        })
        
        let videoRequest = URLRequest(url: url.url)
        let task = self.session.downloadTask(with: videoRequest)
        self.task = task
        task.resume()
    }
    
    fileprivate func LoadOrders() {
        loadingIndicator.startAnimating()
        EnjoyAPI.EnjoyRequest(NConstants.orders,OrderState.self,isHeaders: true) { (orders, errorMessage, error) in
            self.loadingIndicator.stopAnimating()
            if error != nil {
                self.enjoyToast(error!.localizedDescription)
                return
            }
            
            if errorMessage.isEmpty == false {
                self.enjoyToast(errorMessage)
            } else {
                if orders!.success == true {
                    self.orders = orders!.data
                    self.statesCV.isUserInteractionEnabled = true
                    UIView.transition(with: self.OrderStatesCV, duration: 0.5, options: .transitionCrossDissolve, animations: {
                        self.OrderStatesCV.reloadData()
                    }, completion: nil)
                } else {
                    self.enjoyToast(orders!.message)
                }
            }
        }
    }
    
}


extension MyOrdersVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == statesCV {
            return states.count
        } else {
            let rows = (stateSelected) ? selectedState.count : orders.count
            if rows == 0 {
                (currentState == "All") ? showEmptyResultMessage(show: true,status: "") : showEmptyResultMessage(show: true,status: currentState)
            } else {
                showEmptyResultMessage(show: false,status: "")
            }
            return rows
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == statesCV {
            let orderStateCell = collectionView.dequeueReusableCell(withReuseIdentifier: "OrderStateCell", for: indexPath) as! OrderStateCell
            orderStateCell.stateName.text = states[indexPath.row].localized
            return orderStateCell
        } else {
            let stateCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StateCell", for: indexPath) as! StateCell
            stateCell.delegate = self
            stateCell.setupCell(order: (stateSelected) ? selectedState[indexPath.row] : orders[indexPath.row])
            return stateCell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == statesCV {
            if indexPath.row != 0 {
                stateSelected = true
                let state = (states[indexPath.row] != "Completed") ? states[indexPath.row] : "Done"
                currentState = states[indexPath.row]
                selectedState = orders.filter {$0.client_status == state}
            } else {
                stateSelected = false
                currentState = ""
                UIView.transition(with: self.OrderStatesCV, duration: 0.5, options: .transitionCrossDissolve, animations: {
                    self.OrderStatesCV.reloadData()
                }, completion: nil)
            }
        } else {
            let state = (stateSelected) ? selectedState[indexPath.row] : orders[indexPath.row]
            switch state.client_status {
            case "Accepted":
                openEstimationVC(state, accepted: true)
            case "Done":
                self.openVC(storyBoard: "Order", identifier: "OrderComplete")
            case "Pending":
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "OrderDetailsVC") as! OrderDetailsVC
                vc.order = state
                self.navigationController?.pushViewController(vc, animated: true)
            case "In Progress":
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "InProgressVC") as! InProgressVC
                vc.order = state
                self.navigationController?.pushViewController(vc, animated: true)
            case "Rejected":
                openEstimationVC(state,accepted: false)
            default:
                print("Do Nothing")
            }
        }
    }
    
    func openEstimationVC(_ state: OrderStateData, accepted: Bool) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "OrderEstimationVC") as! OrderEstimationVC
        vc.order = state
        vc.state = accepted
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func backToOrders(_ sender: UIStoryboardSegue) {}
}

extension MyOrdersVC: OrderStateDelegate {
    
    func shareVideo(cell: StateCell) {
        self.share(url: cell.order?.video_url ?? "")
    }
    
    func viewVideo(cell: StateCell) {
        let videoViewVC = self.storyboard!.instantiateViewController(withIdentifier: "VideoViewVC") as! VideoViewVC
        videoViewVC.order = cell.order
        self.navigationController?.pushViewController(videoViewVC, animated: true)
    }
    
    func DownloadVideo(cell: StateCell) {
        downLoadEnjoyVideo(url: cell.order?.video_url ?? "")
    }
    
    
}
extension MyOrdersVC: URLSessionDownloadDelegate {
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        let x = String(format:"%.2f", Float(totalBytesWritten) / Float(totalBytesExpectedToWrite))
        self.progress = Float(x)!
        print(progress)
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        let fileManager = FileManager()
        let directoryURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        print(directoryURL)
        let docDirectoryURL = NSURL(fileURLWithPath: "\(directoryURL)")
        let destinationFilename = downloadTask.originalRequest?.url?.lastPathComponent
        let destinationURL =  docDirectoryURL.appendingPathComponent("\(destinationFilename!)")
        
        if let path = destinationURL?.path {
            if fileManager.fileExists(atPath: path) {
                do {
                    try fileManager.removeItem(at: destinationURL!)
                    
                } catch let error as NSError {
                    print(error.debugDescription)
                }
                
            }
        }
        
        do
        {
            try fileManager.copyItem(at: location, to: destinationURL!)
        }
        catch {
            print("Error while copy file")
            
        }
        DispatchQueue.main.async(execute: {() -> Void in
            MBProgressHUD.hide(for: self.view, animated: true)
        })
        let objectsToShare = [destinationURL!]
        let activityVC = UIActivityViewController(activityItems: objectsToShare , applicationActivities: nil)
        activityVC.setValue("Video", forKey: "subject")
        
        activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.assignToContact, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.mail,UIActivity.ActivityType.message, UIActivity.ActivityType.openInIBooks, UIActivity.ActivityType.postToTencentWeibo, UIActivity.ActivityType.postToVimeo, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.print]
        self.present(activityVC, animated: true, completion: nil)
        
    }
}
