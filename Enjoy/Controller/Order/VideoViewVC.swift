//
//  VideoViewVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/31/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import AVKit
import MOLH
import MBProgressHUD

class VideoViewVC: UIViewController {
    
    var order: OrderStateData?
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var videoStatusButton: UIButton!
    @IBOutlet weak var thumbnail: UIImageView!
    var player: AVPlayer?
    var progress: Float = 0.0
    var task: URLSessionTask?
     
    lazy var session : URLSession = {
        return URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: OperationQueue.main)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //load Video
        if order?.video_url?.isEmpty == false {
            let videoURL = URL(string: order?.video_url ?? "")
            player = AVPlayer(url: videoURL!)
            let playerLayer = AVPlayerLayer(player: player)
            playerLayer.frame = self.videoView.frame
            videoView.layer.masksToBounds = true
            playerLayer.videoGravity = AVLayerVideoGravity.resize
            self.videoView.layer.insertSublayer(playerLayer, at: 0)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
        
        self.thumbnail.kf.setImage(with: URL(string: order?.video_thumb_nail?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""),options: [.scaleFactor(UIScreen.main.scale),.transition(.fade(1)),.cacheOriginalImage])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.makeNavTransparent(nav: self.navigationController!)
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "RightArrowLanguage"), style: .plain, target: self, action: #selector(goBack(_:)))
            backButton.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.navigationItem.leftBarButtonItem  = backButton
        }
    }
    
    @objc func playerDidFinishPlaying() {
        videoStatusButton.setImage(#imageLiteral(resourceName: "replay"), for: .normal)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.task?.cancel()
    }
    
    @IBAction func handleVideoState(_ sender: UIButton) {
        if let player = self.player {
            thumbnail.image = nil
            
            if sender.imageView?.image == UIImage(named: "replay") {
                sender.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
                self.player?.seek(to: CMTime.zero)
                self.player?.play()
            } else {
                
                if player.timeControlStatus == AVPlayer.TimeControlStatus.playing {
                    sender.setImage(#imageLiteral(resourceName: "play"), for: .normal)
                    player.pause()
                } else {
                    sender.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
                    player.play()
                }
            }
        }
    }
    
    @IBAction func shareVideo(_ sender: UIButton) {
        self.share(url: order?.video_url ?? "")
    }
    
    @IBAction func downloadVideo(_ sender: UIButton) {
        downLoadEnjoyVideo(url:order?.video_url ?? "")
    }
    
    @objc func cancelVideoDownload() {
        task?.cancel()
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func downLoadEnjoyVideo(url:String) {
        let hud =  MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.mode = .annularDeterminate
        hud.label.text = "Downloading"
        hud.detailsLabel.text = "Cancel"
        let tap = UITapGestureRecognizer(target: self, action: #selector(cancelVideoDownload))
        hud.addGestureRecognizer(tap)
        DispatchQueue.global(qos: .default).async(execute: {() -> Void in
            while self.progress < 1.0 {
                DispatchQueue.main.async(execute: {() -> Void in
                    MBProgressHUD(for: self.view)?.progress = self.progress
                })
                usleep(50000)
            }
        })
        
        let videoRequest = URLRequest(url: url.url)
        let task = self.session.downloadTask(with: videoRequest)
        self.task = task
        task.resume()
    }
}

extension VideoViewVC: URLSessionDownloadDelegate {
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        let x = String(format:"%.2f", Float(totalBytesWritten) / Float(totalBytesExpectedToWrite))
        self.progress = Float(x)!
        print(progress)
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        let fileManager = FileManager()
        let directoryURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        print(directoryURL)
        let docDirectoryURL = NSURL(fileURLWithPath: "\(directoryURL)")
        let destinationFilename = downloadTask.originalRequest?.url?.lastPathComponent
        let destinationURL =  docDirectoryURL.appendingPathComponent("\(destinationFilename!)")
        
        if let path = destinationURL?.path {
            if fileManager.fileExists(atPath: path) {
                do {
                    try fileManager.removeItem(at: destinationURL!)
                    
                } catch let error as NSError {
                    print(error.debugDescription)
                }
            }
        }
        
        do
        {
            try fileManager.copyItem(at: location, to: destinationURL!)
        }
        catch {
            print("Error while copy file")
            
        }
        DispatchQueue.main.async(execute: {() -> Void in
            MBProgressHUD.hide(for: self.view, animated: true)
        })
        let objectsToShare = [destinationURL!]
        let activityVC = UIActivityViewController(activityItems: objectsToShare , applicationActivities: nil)
        activityVC.setValue("Video", forKey: "subject")
        
        activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.assignToContact, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.mail,UIActivity.ActivityType.message, UIActivity.ActivityType.openInIBooks, UIActivity.ActivityType.postToTencentWeibo, UIActivity.ActivityType.postToVimeo, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.print]
        self.present(activityVC, animated: true, completion: nil)
        
    }
}
