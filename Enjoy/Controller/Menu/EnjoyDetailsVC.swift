//
//  EnjoyDetailsVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/15/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import MOLH

class EnjoyDetailsVC: UIViewController {
    
    var isTerms: Bool = false
    
    @IBOutlet weak var notificationIcon: BadgeBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 257
        
        if isTerms {
            tableView.estimatedRowHeight = 85
            self.navigationController!.isNavigationBarHidden = false
            let leftBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "LeftArrow"), style: .done, target: self, action: #selector(goBack(_:)))
            leftBarButton.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            self.navigationItem.leftBarButtonItem = leftBarButton
            applyEnjoyGradient(nav: self.navigationController!)
        } else {
            tableView.estimatedRowHeight = 257
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if notificationIcon != nil {
            notificationIcon.badgeNumber = EnjoyData.shared.getIntValue(Constants.notificationCount)
        }
        
        if isTerms {
            if MOLHLanguage.currentAppleLanguage() == "ar" {
                let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "RightArrowLanguage"), style: .plain, target: self, action: #selector(goBack(_:)))
                backButton.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                self.navigationItem.leftBarButtonItem  = backButton
                
            }
        }
    }
}

extension EnjoyDetailsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isTerms == false {
            let aboutUsCell = tableView.dequeueReusableCell(withIdentifier: "AboutUsCell", for: indexPath) as! EnjoyDetailsCell
            aboutUsCell.setupCell(text: EnjoyData.shared.getStringValue(Constants.aboutApp))
            
            return aboutUsCell
        } else {
            let termsCell = tableView.dequeueReusableCell(withIdentifier: "TermsCell", for: indexPath) as! EnjoyDetailsCell
            termsCell.setupCell(text: EnjoyData.shared.getStringValue(Constants.termsConditions))
            return termsCell
        }
    }
}
