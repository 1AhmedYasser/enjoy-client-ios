//
//  ContactUsVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/15/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import MessageUI

class ContactUsVC: UIViewController, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var notificationIcon: BadgeBarButtonItem!
    @IBOutlet weak var phoneNumber: DesignableButton!
    @IBOutlet weak var email: DesignableButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneNumber.setTitle(EnjoyData.shared.getStringValue(Constants.contactPhoneNumber), for: .normal)
        email.setTitle(EnjoyData.shared.getStringValue(Constants.contactEmail), for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        email.titleLabel!.minimumScaleFactor = 0.5;
        email.titleLabel!.adjustsFontSizeToFitWidth = true;
        notificationIcon.badgeNumber = EnjoyData.shared.getIntValue(Constants.notificationCount)
    }
    
    @IBAction func openSocialLink(_ sender: UIButton) {
        
        switch sender.tag {
        case 0:
            openUrl(url: EnjoyData.shared.getStringValue(Constants.facebookLink))
        case 1:
            openUrl(url: EnjoyData.shared.getStringValue(Constants.twitterLink))
        case 2:
            openUrl(url: EnjoyData.shared.getStringValue(Constants.youtubeLink))
        case 3:
            openUrl(url: EnjoyData.shared.getStringValue(Constants.instagramLink))
        case 4:
            openUrl(url: EnjoyData.shared.getStringValue(Constants.snapchatLink))
        default:
            print("Error")
        }
    }
    
    func openUrl(url: String) {
        if let url = URL(string: url) {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url)
            } else {
                print("Cant open Url")
            }
        }
    }
    
    func phoneCall(to phoneNumber:String) {
        if let callURL:URL = URL(string: "tel:\(phoneNumber)") {
            if (UIApplication.shared.canOpenURL(callURL)) {
                UIApplication.shared.open(callURL)
            }
        }
    }
    
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([email.titleLabel?.text ?? ""])
            present(mail, animated: true)
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    @IBAction func call(_ sender: DesignableButton) {
        phoneCall(to: phoneNumber.titleLabel?.text ?? "")
    }
    @IBAction func email(_ sender: DesignableButton) {
        sendEmail()
    }
}
