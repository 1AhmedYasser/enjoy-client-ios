//
//  EditProfileVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/20/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import MOLH
import NVActivityIndicatorView

class EditProfileVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var userImage: DesignableImageView!
    @IBOutlet weak var userName: DesignableUITextField!
    @IBOutlet weak var phoneNumber: DesignableUITextField!
    @IBOutlet weak var email: DesignableUITextField!
    @IBOutlet weak var country: DesignableButton!
    @IBOutlet weak var city: DesignableButton!
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    
    var countryId: Int = EnjoyData.shared.getIntValue(Constants.userCountryId)
    var cityId: Int = EnjoyData.shared.getIntValue(Constants.userCityId)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadUserData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "RightArrowLanguage"), style: .plain, target: self, action: #selector(goBack(_:)))
            backButton.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.navigationItem.leftBarButtonItem  = backButton
        }
    }
    
    func loadUserData() {
        userImage.kf.setImage(
            with: URL(string: EnjoyData.shared.getStringValue(Constants.userImage)),
            placeholder: userImage.image,
            options: [
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
        ])
        
        userName.text = EnjoyData.shared.getStringValue(Constants.userName)
        phoneNumber.text = EnjoyData.shared.getStringValue(Constants.userPhoneNumber)
        email.text = EnjoyData.shared.getStringValue(Constants.userEmail)
        if EnjoyData.shared.getStringValue(Constants.userCountry).isEmpty {
            country.setTitle("Country", for: .normal)
            country.setTitleColor(UIColor.lightGray, for: .normal)
        } else {
            country.setTitle(EnjoyData.shared.getStringValue(Constants.userCountry), for: .normal)
            country.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        }
        if EnjoyData.shared.getStringValue(Constants.userCity).isEmpty {
            city.setTitle("City", for: .normal)
            city.setTitleColor(UIColor.lightGray, for: .normal)
        } else {
            city.setTitle(EnjoyData.shared.getStringValue(Constants.userCity), for: .normal)
            city.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        }
    }
    
    @IBAction func changeCountry(_ sender: DesignableButton) {
        let countriesVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CountriesVC") as! CountriesVC
        countriesVC.callback = { (country) in
            sender.setTitle(country.name, for: .normal)
            sender.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            self.countryId = country.id
        }
        self.navigationController?.pushViewController(countriesVC, animated: true)
    }
    
    @IBAction func changeCity(_ sender: DesignableButton) {
        let citiesVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CitiesVC") as! CitiesVC
        citiesVC.countryId = self.countryId
        citiesVC.callback = { (city) in
            sender.setTitle(city.name, for: .normal)
            sender.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            self.cityId = city.id
        }
        self.navigationController?.pushViewController(citiesVC, animated: true)
    }
    
    @IBAction func chooseImage(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Set Profile Image".localized, message: "", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Take a picture".localized, style: .default, handler: { action  in
            self.initializeImagePicker(source: .camera)
        }))
        
        alert.addAction(UIAlertAction(title: "Choose from gallery".localized, style: .default, handler: {action in
            self.initializeImagePicker(source: .photoLibrary)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel".localized, style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    @IBAction func updateUserInfo(_ sender: UIButton) {
        sender.isEnabled = false
        if userName.text!.isEmpty || email.text!.isEmpty || phoneNumber.text!.isEmpty {
            enjoyToast("Please fill the empty fields".localized)
            sender.isEnabled = true
            return
        }
        
        let parameters = ["id": EnjoyData.shared.getIntValue(Constants.userId),
                          "username": userName.text ?? "",
                          "email": email.text ?? "",
                          "phone_number": phoneNumber.text ?? "",
                          "image": "\(convertImageToBase64(userImage.image!))",
                          "country_id": countryId,
                          "city_id": cityId] as [String : Any]
        loadingIndicator.startAnimating()
        
        EnjoyAPI.EnjoyRequest(NConstants.editProfile,User.self,parameters,isHeaders: true,.post) { (user, errorMessage, error) in
            self.loadingIndicator.stopAnimating()
            sender.isEnabled = true
            if error != nil {
                self.enjoyToast(error!.localizedDescription)
                return
            }
            
            if errorMessage.isEmpty == false {
                self.enjoyToast(errorMessage)
            } else {
                if user!.success == true {
                    EnjoyData.shared.SaveUserInfo(user: user?.data,withToken: false)
                    self.goBack(sender)
                } else {
                    self.enjoyToast(user!.message)
                }
            }
        }
    }
    
    func initializeImagePicker(source: UIImagePickerController.SourceType){
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = source
        imagePicker.allowsEditing = true
        present(imagePicker,animated: true,completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let retrievedImage = info[.editedImage] as? UIImage {
            userImage.image = retrievedImage
        } else if let retrievedImage = info[.originalImage] as? UIImage {
            userImage.image = retrievedImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    // Convert Image to base64 string
    func convertImageToBase64(_ image: UIImage) -> String {
        let imageData:NSData = image.jpegData(compressionQuality: 0.4)! as NSData
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        return strBase64
    }
}
