//
//  ArtistDetailsVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/25/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import AVKit
import MOLH

class ArtistDetailsVC: UIViewController, ArtistCellDelegate {
    
    var artist: ArtistsData?
    @IBOutlet weak var artistImage: UIImageView!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var artistVideosNo: UILabel!
    @IBOutlet weak var artistPrice: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var notificationIcon: BadgeBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadArtistInfo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        notificationIcon.badgeNumber = EnjoyData.shared.getIntValue(Constants.notificationCount)
        
        self.makeNavTransparent(nav: self.navigationController!)
        
        
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "RightArrowLanguage"), style: .plain, target: self, action: #selector(goBack(_:)))
            backButton.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.navigationItem.leftBarButtonItem  = backButton
        }
        
    }
    
    fileprivate func loadArtistInfo() {
        if let artist = self.artist {
            artistImage!.kf.setImage(with: URL(string: artist.image?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)! ?? ""),placeholder: UIImage(named: "Guest"),options: [.scaleFactor(UIScreen.main.scale),.transition(.fade(1)),.cacheOriginalImage])
            artistName.text = artist.name
            artistVideosNo.text = "\(artist.videos_no)"
            artistPrice.text = "\(artist.price) $"
        }
    }
    
    @IBAction func requestService(_ sender: UIButton) {
        if EnjoyData.shared.getStringValue(Constants.userToken).isEmpty {
            let vc = UIStoryboard(name: "Common", bundle: nil).instantiateViewController(withIdentifier: "LoginRequestVC") as! LoginRequestVC
            vc.canBack = true
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = UIStoryboard(name: "Order", bundle: nil).instantiateViewController(withIdentifier: "RequestServiceVC") as! RequestServiceVC
            vc.passedArtist = self.artist
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func fullScreenPressed(cell: ArtistVideoCell) {
        if cell.player != nil {
            let playerViewController = AVPlayerViewController()
            playerViewController.player = cell.player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
    }
}

extension ArtistDetailsVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return artist?.profile_videos.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let videoCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ArtistVideoCell", for: indexPath) as! ArtistVideoCell
        videoCell.delegate = self
        let video = artist?.profile_videos[indexPath.row]
        videoCell.setupVideo(video: video?.video ?? "",thumbnail: video?.thumb_nail ?? "",title: video?.title ?? "")
        
        return videoCell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerCell = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "ArtistPromoHeader", for: indexPath) as! ArtistPromoHeader
        headerCell.setupVideo(video: artist?.promo_video ?? "")
        return headerCell
    }
}
