//
//  ArtistsVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/16/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import AnimatableReload

class ArtistsVC: UIViewController, ArtistCellDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var artistsTable: UITableView!
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    @IBOutlet weak var notificationIcon: BadgeBarButtonItem!
    
    var artists = [ArtistsData]()
    
    var filter = [ArtistsData]()
    
    var isFiltering: Bool  = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadingIndicator.startAnimating()
        EnjoyAPI.EnjoyRequest(NConstants.artists,clients: false, Artists.self,isHeaders: false) { (artists, errorMessage, error) in
            self.loadingIndicator.stopAnimating()
            if error != nil {
                self.enjoyToast(error!.localizedDescription)
                return
            }
            
            if errorMessage.isEmpty == false {
                self.enjoyToast(errorMessage)
            } else {
                if artists!.success == true {
                    self.artists = artists!.data
                    AnimatableReload.reload(tableView: self.artistsTable, animationDirection: "left")
                } else {
                    self.enjoyToast(artists!.message)
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        notificationIcon.badgeNumber = EnjoyData.shared.getIntValue(Constants.notificationCount)
        
        applyEnjoyGradient(nav: self.navigationController!,searchBar: searchBar)
    }
    
    func requestServicePressed(cell: ArtistCell) {
        if EnjoyData.shared.getStringValue(Constants.userToken).isEmpty {
            let vc = UIStoryboard(name: "Common", bundle: nil).instantiateViewController(withIdentifier: "LoginRequestVC") as! LoginRequestVC
            vc.canBack = true
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = UIStoryboard(name: "Order", bundle: nil).instantiateViewController(withIdentifier: "RequestServiceVC") as! RequestServiceVC
            vc.passedArtist = cell.artist
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}

extension ArtistsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let rows = (isFiltering) ? filter.count : artists.count
        return rows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let artistCell = tableView.dequeueReusableCell(withIdentifier: "ArtistCell", for: indexPath) as! ArtistCell
        (isFiltering == false) ? artistCell.setupCell(artist: artists[indexPath.row]) : artistCell.setupCell(artist: filter[indexPath.row])
        artistCell.delegate = self
        return artistCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "ArtistDetailsVC") as! ArtistDetailsVC
        vc.artist = artists[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension ArtistsVC : UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.isFiltering = true
        filter = artists.filter({ (artist) -> Bool in
            return artist.name.starts(with: searchText) || artist.name.starts(with: searchText.uppercased()) ||
                artist.name.uppercased().starts(with: searchText.uppercased())})
        artistsTable.reloadData()
    }
}

