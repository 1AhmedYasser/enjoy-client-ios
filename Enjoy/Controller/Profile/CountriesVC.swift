//
//  CountriesVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/11/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Kingfisher
import MOLH

class CountriesVC: UIViewController {
    
    @IBOutlet weak var countriesTable: UITableView!
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    
    var countries = [CountriesData]() {
        didSet {
            self.countriesTable.reloadData()
        }
    }
    
    var filter = [CountriesData](){
        didSet{
            self.countriesTable.reloadData()
        }
    }
    
    var isFiltering: Bool  = false
    
    var callback : ((CountriesData)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController!.isNavigationBarHidden = false
        applyEnjoyGradient(nav: self.navigationController!)
        loadingIndicator.startAnimating()
        EnjoyAPI.EnjoyRequest(NConstants.countries,clients: false,Countries.self,isHeaders: false) { (countries, errorMessage, error) in
            self.loadingIndicator.stopAnimating()
            if error != nil {
                self.enjoyToast(error!.localizedDescription)
                return
            }
            
            if errorMessage.isEmpty == false {
                self.enjoyToast(errorMessage)
            } else {
                if countries!.success == true {
                    self.countries = countries!.data
                } else {
                    self.enjoyToast(countries!.message)
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "RightArrowLanguage"), style: .plain, target: self, action: #selector(goBack(_:)))
            backButton.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.navigationItem.leftBarButtonItem  = backButton
        }
    }
}

extension CountriesVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let rows = (isFiltering) ? filter.count : countries.count
        return rows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let countryCell = tableView.dequeueReusableCell(withIdentifier: "CountryCell", for: indexPath) as! CountryCell
        (isFiltering) ? countryCell.setupCell(country: filter[indexPath.row]) : countryCell.setupCell(country: countries[indexPath.row])
        return countryCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCountry = (isFiltering) ? filter[indexPath.row] : countries[indexPath.row]
        callback?(selectedCountry)
        self.navigationController?.popViewController(animated: true)
    }
}

extension CountriesVC : UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.isFiltering = true
        filter = countries.filter({ (country) -> Bool in
            return country.name.starts(with: searchText) || country.name.starts(with: searchText.uppercased()) ||
                country.name.uppercased().starts(with: searchText.uppercased())})
        countriesTable.reloadData()
    }
}
