//
//  CitiesVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/11/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import MOLH

class CitiesVC: UIViewController {
    
    @IBOutlet weak var citiesTable: UITableView!
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    
    var cities = [CitiesData]()
    var countryId = 0
    
    var callback : ((CitiesData)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController!.isNavigationBarHidden = false
        applyEnjoyGradient(nav: self.navigationController!)
        loadingIndicator.startAnimating()
        let parameters = ["country_id":countryId]
        EnjoyAPI.EnjoyRequest(NConstants.cities,clients: false,Cities.self,parameters,isHeaders: false) { (cities, errorMessage, error) in
            self.loadingIndicator.stopAnimating()
            if error != nil {
                self.enjoyToast(error!.localizedDescription)
                return
            }
            
            if errorMessage.isEmpty == false {
                self.enjoyToast(errorMessage)
            } else {
                if cities!.success == true {
                    self.cities = cities!.data
                    self.citiesTable.reloadData()
                } else {
                    self.enjoyToast(cities!.message)
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "RightArrowLanguage"), style: .plain, target: self, action: #selector(goBack(_:)))
            backButton.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.navigationItem.leftBarButtonItem  = backButton
        }
    }
}

extension CitiesVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cityCell = tableView.dequeueReusableCell(withIdentifier: "CityCell", for: indexPath)
        cityCell.textLabel?.text = cities[indexPath.row].name
        return cityCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        callback?(cities[indexPath.row])
        self.navigationController?.popViewController(animated: true)
    }
}
