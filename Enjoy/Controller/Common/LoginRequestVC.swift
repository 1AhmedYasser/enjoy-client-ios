//
//  LoginRequestVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/15/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit

class LoginRequestVC: UIViewController {

    var canBack: Bool = false
    var appliedGradient = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if canBack {
            let leftBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "LeftArrow"), style: .done, target: self, action: #selector(goBack(_:)))
            leftBarButton.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            self.navigationItem.leftBarButtonItem = leftBarButton
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.navigationController!.navigationBar.isTranslucent {
            applyEnjoyGradient(nav: self.navigationController!)
            appliedGradient = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if appliedGradient {
            self.makeNavTransparent(nav: self.navigationController!)
        }
    }

    @IBAction func goToLogin(_ sender: Any) {
        openVC(storyBoard: "Authentication", identifier: "AuthVC")
    }
}
