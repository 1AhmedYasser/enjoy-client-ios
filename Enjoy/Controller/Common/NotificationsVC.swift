//
//  NotificationsVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 1/9/20.
//  Copyright © 2020 Ahmed yasser. All rights reserved.
//

import UIKit
import EmptyDataSet_Swift
import MOLH
import NVActivityIndicatorView

class NotificationsVC: UIViewController {
    
    var appliedGradient = false
    var notifications = [NotificationsData]()
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if EnjoyData.shared.getStringValue(Constants.userToken).isEmpty {
            showEmptyResultMessage(show: true)
        } else {
            loadNotifications()
            markAllRead()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.navigationController!.navigationBar.isTranslucent {
            applyEnjoyGradient(nav: self.navigationController!)
            appliedGradient = true
        }
        
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "RightArrowLanguage"), style: .plain, target: self, action: #selector(goBack(_:)))
            backButton.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.navigationItem.leftBarButtonItem  = backButton
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if appliedGradient {
            self.makeNavTransparent(nav: self.navigationController!)
        }
    }
    
    func loadNotifications() {
        loadingIndicator.startAnimating()
        showEmptyResultMessage(show: false)
        EnjoyAPI.EnjoyRequest(NConstants.enjoyNotifications,Notifications.self,isHeaders: true) { (notifications, errorMessage, error) in
            self.loadingIndicator.stopAnimating()
            if error != nil {
                 self.enjoyToast(error!.localizedDescription)
                return
            }
            
            if errorMessage.isEmpty == false {
                 self.enjoyToast(errorMessage)
            } else {
                if notifications!.success == true {
                    (notifications!.data.count != 0) ? (self.notifications = notifications!.data) : (self.showEmptyResultMessage(show: true))
                    UIView.transition(with: self.collectionView, duration: 0.5, options: .transitionCrossDissolve, animations: {
                        self.collectionView.reloadData()
                    }, completion: nil)
                } else {
                     self.enjoyToast(notifications!.message)
                }
            }
        }
    }
    
    func markAllRead() {
        EnjoyAPI.EnjoyRequest("\(NConstants.markRead)/\(EnjoyData.shared.getIntValue(Constants.userId))/Client",clients: false,EnjoyError.self,isHeaders: true,.post) { (message, errorMessage, error) in
            self.loadingIndicator.stopAnimating()
            if error != nil {
                //self.enjoyToast(error!.localizedDescription)
                return
            }
            
            if errorMessage.isEmpty == false {
                //  self.enjoyToast(errorMessage)
            } else {
                if message!.success == true {
                    print(message!.message)
                    self.save(0, Constants.notificationCount)
                } else {
                    //  self.enjoyToast(message!.message)
                }
            }
        }
    }
    
    func showEmptyResultMessage(show: Bool) {
        if show {
            collectionView.emptyDataSetView { (view) in
                view.image(UIImage(named: "NoNotifications")).detailLabelString(NSAttributedString(string: "No Notifications".localized)).shouldFadeIn(true).verticalSpace(37.2)
            }
        } else {
            collectionView.emptyDataSetView { (view) in
                view.detailLabelString(NSAttributedString(string: ""))
                
            }
        }
    }
}

extension NotificationsVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let notificationCell = collectionView.dequeueReusableCell(withReuseIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        if MOLHLanguage.currentAppleLanguage() == "en" {
            notificationCell.notificationMessage.textAlignment = .left
        } else {
            notificationCell.notificationMessage.textAlignment = .right
        }
        notificationCell.setupCell(notification: notifications[indexPath.row])
        return notificationCell
    }
}
