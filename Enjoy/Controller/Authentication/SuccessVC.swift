//
//  SuccessVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/12/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit

class SuccessVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func goToHome(_ sender: DesignableButton) {
        openVC(storyBoard: "Home", identifier: "HomeMainVC")
    }
}
