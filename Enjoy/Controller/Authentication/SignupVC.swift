//
//  SignupVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/10/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import MOLH
import NVActivityIndicatorView

class SignupVC: UIViewController {
    
    @IBOutlet weak var fullName: DesignableUITextField!
    @IBOutlet weak var phoneNumber: DesignableUITextField!
    @IBOutlet weak var mail: DesignableUITextField!
    @IBOutlet weak var password: DesignableUITextField!
    @IBOutlet weak var confirmPassword: DesignableUITextField!
    @IBOutlet weak var countryBtn: DesignableButton!
    @IBOutlet weak var cityBtn: DesignableButton!
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    @IBOutlet weak var countryImage: UIImageView!
    @IBOutlet weak var countryCode: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    var didAgree:Bool = false
    
    var countryId: Int?
    var cityId: Int? = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            backButton.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
    
    @IBAction func chooseCountry(_ sender: DesignableButton) {
        let countriesVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CountriesVC") as! CountriesVC
        countriesVC.callback = { (country) in
            self.countryBtn.setTitle(country.name, for: .normal)
            self.countryBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            self.countryId = country.id
            self.countryCode.text = country.code
            self.countryImage!.kf.setImage(with: URL(string: country.image),options: [.scaleFactor(UIScreen.main.scale),.transition(.fade(1)),.cacheOriginalImage])
        }
        self.navigationController?.pushViewController(countriesVC, animated: true)
    }
    
    @IBAction func chooseCity(_ sender: DesignableButton) {
        let citiesVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CitiesVC") as! CitiesVC
        citiesVC.countryId = self.countryId ?? 0
        citiesVC.callback = { (city) in
            self.cityBtn.setTitle(city.name, for: .normal)
            self.cityBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            self.cityId = city.id
        }
        self.navigationController?.pushViewController(citiesVC, animated: true)
    }
    
    @IBAction func agreeOnTerms(_ sender: UIButton) {
        didAgree = !didAgree
        (didAgree) ? sender.setImage(#imageLiteral(resourceName: "Tick"), for: .normal) : sender.setImage(#imageLiteral(resourceName: "EmptyTick"), for: .normal)
    }
    
    @IBAction func openTerms(_ sender: UIButton) {
        let termsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "EnjoyDetails") as! EnjoyDetailsVC
        termsVC.isTerms = true
        termsVC.navigationItem.title = "Terms and Conditions".localized
        termsVC.navigationItem.leftBarButtonItems?.removeAll()
        termsVC.navigationItem.rightBarButtonItems?.removeAll()
        termsVC.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.navigationController?.pushViewController(termsVC, animated: true)
    }
    
    @IBAction func signup(_ sender: UIButton) {
        if fullName.text!.isEmpty || phoneNumber.text!.isEmpty || mail.text!.isEmpty || password.text!.isEmpty || confirmPassword.text!.isEmpty || countryId == nil {
            enjoyToast("Please fill the empty fields".localized)
            return
        }
        
        if isValidEmail(mail) == false {
            enjoyToast("Your Email Address is not valid".localized)
            return
        }
        
        if password.text! != confirmPassword.text! {
           enjoyToast("Passwords does not match".localized)
           return
        }
        
        if didAgree == false {
            enjoyToast("You must agree on the terms and conditions".localized)
            return
        }
        
        let parameters = ["username":fullName.text!,
                          "phone_number":phoneNumber.text!,
                          "email":mail.text!,
                          "password":password.text!,
                          "country_id":(countryId == 0) ? "" : countryId!,
                          "city_id":(cityId == 0) ? "" : cityId!,
                          "firebase_token":EnjoyData.shared.getStringValue(Constants.fcmToken)] as [String : Any]
        
        let verificationVC = self.storyboard!.instantiateViewController(withIdentifier: "PhoneVerificationVC") as! PhoneVerificationVC
        verificationVC.parameters = parameters
        verificationVC.countryCode = countryCode.text!
        verificationVC.registerError = { (error) in
            self.enjoyToast(error)
        }
        self.navigationController?.pushViewController(verificationVC, animated: true)
    }
    
    @IBAction func goToLogin(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
