//
//  PhoneVerificationVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 1/2/20.
//  Copyright © 2020 Ahmed yasser. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import PinCodeInputView
import FirebaseAuth
import MOLH

class PhoneVerificationVC: UIViewController {
    
    var parameters: [String:Any]?
    var countryCode: String = ""
    @IBOutlet weak var PinCodeView: UIView!
    @IBOutlet weak var verificationLabel: UILabel!
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    @IBOutlet weak var backButton: UIButton!
    
    var verificationCode: String = ""
    var isVerified = false
    
    var registerError : ((String)->())?
    
    let pinCodeInputView: PinCodeInputView<ItemView> = .init(
        digit: 6,
        itemSpacing: 8,
        itemFactory: {
            return ItemView()
    })
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sendCode()
        setupPinCodeView(view: PinCodeView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            backButton.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
    
    func sendCode() {
        PhoneAuthProvider.provider().verifyPhoneNumber("\(countryCode)\(parameters!["phone_number"] ?? 0)", uiDelegate: nil) { (verificationId, error) in
            if error != nil {
                self.enjoyToast(error!.localizedDescription)
                return
            }
            
            guard let verificationId = verificationId else { return }
            self.save(verificationId, Constants.verificationCode)
            print(self.verificationCode)
        }
    }
    
    func setupPinCodeView(view: UIView) {
        
        self.view.addSubview(pinCodeInputView)
        pinCodeInputView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 50)
        pinCodeInputView.center = view.center
        // set appearance
        pinCodeInputView.set(
            appearance: .init(
                itemSize: .init(width: 43, height: 43),
                font: .systemFont(ofSize: 28, weight: .bold),
                textColor: #colorLiteral(red: 0.8235294118, green: 0.1921568627, blue: 0.5098039216, alpha: 1),
                backgroundColor: .white,
                cursorColor: #colorLiteral(red: 0.8235294118, green: 0.1921568627, blue: 0.5098039216, alpha: 1),
                cornerRadius: 8
            )
        )
        pinCodeInputView.layer.shadowColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        pinCodeInputView.layer.shadowOffset = CGSize(width: 0, height: 0.5)
        pinCodeInputView.layer.shadowOpacity = 0.5
        pinCodeInputView.layer.shadowRadius = 5
        
        // text handling
        pinCodeInputView.set(changeTextHandler: { text in
            if text.count == 6 {
                self.verificationCode = text
                self.pinCodeInputView.layer.shadowColor = #colorLiteral(red: 0, green: 0.6347020268, blue: 0.008689996786, alpha: 1)
            } else {
                self.verificationCode = text
                self.pinCodeInputView.layer.shadowColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            }
        })
    }
    
    @IBAction func verify(_ sender: UIButton) {
        if self.verificationCode.count == 6 {
            loadingIndicator.startAnimating()
            let credential = PhoneAuthProvider.provider().credential(withVerificationID: EnjoyData.shared.getStringValue(Constants.verificationCode),verificationCode: pinCodeInputView.text)
            Auth.auth().signIn(with: credential) { (result, error) in
                if error != nil {
                    self.loadingIndicator.stopAnimating()
                    self.pinCodeInputView.layer.shadowColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
                    if error!.localizedDescription.contains("invalid") {
                        self.enjoyToast("Wrong verification Code".localized)
                    } else {
                        self.enjoyToast("\(error!.localizedDescription)")
                    }
                    return
                }
                
                print(result ?? "")
                self.register(sender)
            }
        } else {
            self.enjoyToast("Wrong verification Code".localized)
        }
    }
    
    @IBAction func resendCode(_ sender: UIButton) {
        self.enjoyToast("New verification Code sent".localized)
        sendCode()
    }
    
    func register(_ sender: UIButton) {
        if let parameters = self.parameters {
            sender.isEnabled = false
            EnjoyAPI.EnjoyRequest(NConstants.register,User.self,parameters,isHeaders: false,.post) { (user, errorMessage, error) in
                self.loadingIndicator.stopAnimating()
                sender.isEnabled = true
                if error != nil {
                    self.enjoyToast(error!.localizedDescription)
                    return
                }
                
                if errorMessage.isEmpty == false {
                    self.registerError?(errorMessage)
                    self.goBack(sender)
                } else {
                    if user!.success == true {
                        EnjoyData.shared.SaveUserInfo(user: user?.data)
                        self.openVC(storyBoard: "Authentication", identifier: "SuccessVC")
                    } else {
                        self.registerError?(errorMessage)
                        self.goBack(sender)
                    }
                }
            }
        } else {
            print("Error signing up")
        }
    }
}
