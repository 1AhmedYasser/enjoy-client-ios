//
//  EnjoyNavigationBar.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/15/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit

class EnjoyNavigationBar: UINavigationController {
    
    var checkForLogin:Bool = true
    var isTransparent:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if checkForLogin == true {
            if EnjoyData.shared.getStringValue(Constants.userToken).isEmpty {
                let skipController = UIStoryboard(name: "Common", bundle: nil).instantiateViewController(withIdentifier: "LoginRequestVC")
                self.setViewControllers([skipController], animated: false)
            }
            self.navigationBar.barStyle = .black
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isTransparent == false {
            let gradientLayer = CAGradientLayer()
            let updatedFrame = self.navigationBar.bounds
            gradientLayer.frame = updatedFrame
            gradientLayer.colors = [UIColor(hex: 0x4b0965).cgColor, UIColor(hex: 0xa7386f).cgColor]
            gradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
            gradientLayer.endPoint = CGPoint(x: 1, y: 0.5)
            
            UIGraphicsBeginImageContext(gradientLayer.bounds.size)
            gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            self.navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
        } else {
            self.navigationBar.setBackgroundImage(UIImage(), for: .default)
            self.navigationBar.shadowImage = UIImage()
            self.navigationBar.isTranslucent = true
            self.view.backgroundColor = .clear
        }
        
        if checkForLogin == true {
            if EnjoyData.shared.getStringValue(Constants.userToken).isEmpty {
                let skipController = UIStoryboard(name: "Common", bundle: nil).instantiateViewController(withIdentifier: "LoginRequestVC")
                self.setViewControllers([skipController], animated: false)
            }
        }
        self.navigationBar.barStyle = .black
    }
}

