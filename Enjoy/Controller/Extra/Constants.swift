//
//  Constants.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/12/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import Foundation

class Constants {
    
    // MARK: User
    static let userId: String = "UserId"
    static let userName: String = "UserName"
    static let userEmail: String = "UserEmail"
    static let userCountryId: String = "UserCountryId"
    static let userCityId: String = "UserCityId"
    static let userPhoneNumber: String = "UserPhoneNumber"
    static let userImage: String = "UserImage"
    static let userValid: String = "UserValid"
    static let userToken: String = "UserToken"
    static let userCountry: String = "UserCountry"
    static let userCity: String = "UserCity"
    static let fcmToken: String = "FcmToken"
    
    static let userLogged: String = "UserLogged"
    static let openedEnjoyBefore: String = "FirstTime"
    static let verificationCode:String = "VerificationCode"
    
    // MARK: Contact Info
    static let contactEmail: String = "ContactEmail"
    static let contactPhoneNumber: String = "ContactPhoneNumber"
    static let facebookLink: String = "FacebookLink"
    static let twitterLink: String = "TwitterLink"
    static let youtubeLink: String = "YouTubeLink"
    static let instagramLink: String = "InstagramLink"
    static let snapchatLink: String = "SnapchatLink"
    
    // MARK: Enjoy Data
    static let aboutApp: String = "AboutApp"
    static let termsConditions: String = "TermsConditions"
    static let privacyPolicy: String = "PrivacyPolicy"
    static let shareLink: String = "ShareLink"
    static let notificationCount: String = "NotificationCount"
}
