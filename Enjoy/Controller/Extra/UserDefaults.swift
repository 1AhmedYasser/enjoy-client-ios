//
//  UserDefaults.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/12/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import Foundation

class EnjoyData : UserDefaults {
    
    static let shared = EnjoyData()
    func SaveUserInfo (user : UserData?,withToken: Bool = true) {
        if withToken == true {
            UserDefaults.standard.set("Bearer \(user?.token ?? "")", forKey: Constants.userToken)
        }
        UserDefaults.standard.set(user?.username, forKey: Constants.userName)
        UserDefaults.standard.set(user?.email, forKey: Constants.userEmail)
        UserDefaults.standard.set(user?.phone_number, forKey: Constants.userPhoneNumber)
        UserDefaults.standard.set(user?.image, forKey: Constants.userImage)
        UserDefaults.standard.set(user?.country_id, forKey: Constants.userCountryId)
        UserDefaults.standard.set(user?.city_id, forKey: Constants.userCityId)
        UserDefaults.standard.set(user?.country_text, forKey: Constants.userCountry)
        UserDefaults.standard.set(user?.city_text, forKey: Constants.userCity)
        UserDefaults.standard.set(user?.id, forKey: Constants.userId)
        UserDefaults.standard.set(user?.valid, forKey: Constants.userValid)
    }
    
    func saveContactInfo(info: ContactUsData) {
        UserDefaults.standard.set(info.contact_email, forKey: Constants.contactEmail)
        UserDefaults.standard.set(info.phone_number, forKey: Constants.contactPhoneNumber)
        UserDefaults.standard.set(info.facebook_link, forKey: Constants.facebookLink)
        UserDefaults.standard.set(info.twitter_link, forKey: Constants.twitterLink)
        UserDefaults.standard.set(info.youtube_link, forKey: Constants.youtubeLink)
        UserDefaults.standard.set(info.instgram_link, forKey: Constants.instagramLink)
        UserDefaults.standard.set(info.snapchat_link, forKey: Constants.snapchatLink)
    }
    
    func saveDetails(data: DetailsInnerData) {
        UserDefaults.standard.set(data.about_app, forKey: Constants.aboutApp)
        UserDefaults.standard.set(data.terms_conditions, forKey: Constants.termsConditions)
        UserDefaults.standard.set(data.privacy_policy, forKey: Constants.privacyPolicy)

    }
    
    func getStringValue(_ key: String) -> String {
        return UserDefaults.standard.string(forKey: key) ?? ""
    }
    func getIntValue(_ key: String) -> Int {
        return UserDefaults.standard.integer(forKey: key)
    }
    func getBoolValue(_ key: String) -> Bool {
        return UserDefaults.standard.bool(forKey: key)
    }
    func getDoubleValue(_ key: String) -> Double {
        return UserDefaults.standard.double(forKey: key)
    }
    
    func isUserLoggedIn() -> Bool {
        return getBoolValue(Constants.userLogged)
    }
    
    func openedEnjoyBefore() -> Bool {
        return getBoolValue(Constants.openedEnjoyBefore)
    }
}
