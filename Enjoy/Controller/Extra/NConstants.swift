//
//  NConstants.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/10/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import Foundation

class NConstants {
    
    static let EnjoyBase:String = "https://emenjoy.com/old/Enjoy/public/api/"
    
    static let Clients: String = "clients/"
    static let Public: String = "public/"
    
    // Endpoints
    static let login: String = "login"
    static let register:String = "register"
    static let forgetPassword:String = "forget_password"
    static let changePassword:String = "change_password"
    static let editProfile:String = "edit_profile"
    static let logout:String = "logout"
    static let countries:String = "countries"
    static let cities:String = "cities"
    static let contactUs:String = "contact"
    static let data:String = "data"
    static let artists:String = "artists"
    static let home:String = "home"
    static let categories:String = "categories"
    static let likes:String = "likes"
    static let category:String = "category"
    static let orders:String = "orders"
    static let placeOrder:String = "place_order"
    static let cancelOrder:String = "cancel_order"
    static let payOrder:String = "pay_order"
    static let notification:String = "notification"
    static let enjoyNotifications:String = "notifications"
    static let notificationsCount:String = "notifications_count"
    static let markRead:String = "mark_read"
    
    static func endpoint(_ endpoint: String,_ clients: Bool = true) -> URL {
        let type = (clients) ? Clients : Public
        return "\(EnjoyBase)\(type)\(endpoint)".url
    }
}

