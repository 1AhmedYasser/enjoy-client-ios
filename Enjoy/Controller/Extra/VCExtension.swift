//
//  VCExtension.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/22/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import Foundation
import UIKit
import Toast_Swift
import MOLH
import Malert
import NVActivityIndicatorView

extension UIViewController {
    
    func enjoyToast(_ message: String) {
        var style = ToastStyle()
        style.messageAlignment = .center
        self.view.makeToast(message,style: style)
    }
    
    func isValidEmail(_ emailField: UITextField) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with:emailField.text )
    }
    
    @IBAction func openMenu(_ sender: UIBarButtonItem) {
        if MOLHLanguage.currentAppleLanguage() == "en" {
            self.presentLeftMenuViewController(sender)
        } else {
            self.presentRightMenuViewController(sender)
        }
    }
    
    @IBAction func goBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func popController(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func openNotifications(_ sender: UIBarButtonItem) {
        self.openVC(storyBoard: "Notification", identifier: "NotificationsVC")
    }
    
    func share(url: String) {
        let url = url
        let controller = UIActivityViewController(activityItems: [url], applicationActivities: nil)
        controller.completionWithItemsHandler = {(activityType: UIActivity.ActivityType?,completed: Bool, returnedItems:[Any]?,error: Error?) in
            if completed {
                controller.dismiss(animated: true, completion: nil)
            } else {
                return
            }
        }
        present(controller, animated: true, completion: nil)
    }
    
    func enjoyAlert(image: UIImage,message: String,vc: UIViewController, completionHandler: @escaping () -> Void) {
        let enjoyAlert = EnjoyPopup.instantiateFromNib()
        
        enjoyAlert.setupAlert(image: image, message: message,vc: vc)
                
        let alert = Malert(customView: enjoyAlert)
        
        let height:NSLayoutConstraint = NSLayoutConstraint(item: enjoyAlert, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 180)

        enjoyAlert.addConstraint(height)
        alert.animationType = .fadeIn
        alert.cornerRadius = 20
        
        alert.onDismissMalert {
            completionHandler()
        }
        
        let action = MalertAction(title: "OK".localized)

        action.tintColor = .white
        action.backgroundColor = #colorLiteral(red: 0.8235294118, green: 0.1921568627, blue: 0.5098039216, alpha: 1)
        alert.buttonsHeight = 50

        alert.addAction(action)
        
        present(alert, animated: true)
    }
    
    func openVC(storyBoard: String,identifier: String) {
        let vc = UIStoryboard(name: storyBoard, bundle: nil).instantiateViewController(withIdentifier: identifier)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func save(_ value: Any?,_ key: String) {
        UserDefaults.standard.set(value, forKey: key)
    }
    
    func logout(sender: UIButton,loadingIndicator: NVActivityIndicatorView) {
        print(MOLHLanguage.currentAppleLanguage())
        print(MOLHLanguage.currentAppleLanguage())
        print(MOLHLanguage.currentLocaleIdentifier())
        let alert = UIAlertController(title: "Are you sure you want to logout ?".localized, message: "", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Yes".localized, style: .default, handler: { action  in
            sender.isEnabled = false
            loadingIndicator.startAnimating()
            EnjoyAPI.EnjoyRequest(NConstants.logout,EnjoyError.self,isHeaders: true,.post) { (message, errorMessage, error) in
                loadingIndicator.stopAnimating()
                sender.isEnabled = true
                if error != nil {
                    print(error!.localizedDescription)
                }
                
                if errorMessage.isEmpty == false {
                    print(errorMessage)
                } else {
                    if message!.success == true {
                        print(message!.message)
                    } else {
                        print(message!.message)
                    }
                }
                
                // Logout on faliure or success
                self.save("", Constants.userToken)
                self.save("",Constants.userId)
                self.save(false, Constants.userLogged)
                let vc = UIStoryboard(name: "Authentication", bundle: nil).instantiateViewController(withIdentifier: "AuthVC")
                let contentVC = UINavigationController(rootViewController: vc)
                contentVC.navigationBar.barStyle = .black
                self.sideMenuViewController!.setContentViewController(contentVC, animated: true)
                self.sideMenuViewController!.hideMenuViewController()
                self.sideMenuViewController!.panGestureEnabled = false
               // self.openVC(storyBoard: "Authentication", identifier: "AuthVC")
            }
        }))
        
        alert.addAction(UIAlertAction(title: "No".localized, style: .default, handler: nil))
        
        self.present(alert, animated: true)
    
    }
    
    func like(cell: VideoCell?,sender:UIButton, likesLabel:UILabel ,likesImage: UIImageView ,videoId: Int,loadingIndicator: NVActivityIndicatorView,isButton: Bool = false) {
        if EnjoyData.shared.getStringValue(Constants.userToken).isEmpty {
            enjoyToast("Please Login to like the video".localized)
        } else {
            if isButton == false {
                if likesImage.image == #imageLiteral(resourceName: "thumbs_up_filled") {
                    likesImage.image = #imageLiteral(resourceName: "thumbsUp")
                    cell?.isLiked = false
                    likesLabel.text = "\(Int(likesLabel.text!)! - 1)"
                } else {
                    likesImage.image = #imageLiteral(resourceName: "thumbs_up_filled")
                    cell?.isLiked = true
                    likesLabel.text = "\(Int(likesLabel.text!)! + 1)"
                }
            } else {
                if cell?.likesImage.image == #imageLiteral(resourceName: "thumbs_up_filled") {
                    cell?.likesImage.image =  #imageLiteral(resourceName: "thumbsUp")
                    cell?.isLiked = false
                    likesLabel.text = "\(Int(likesLabel.text!)! - 1)"
                } else {
                    cell?.likesImage.image = #imageLiteral(resourceName: "thumbs_up_filled")
                    cell?.isLiked = true
                    likesLabel.text = "\(Int(likesLabel.text!)! + 1)"
                }
            }
            
            let parameters = ["video_id":videoId]
            loadingIndicator.startAnimating()
            sender.isEnabled = false
            EnjoyAPI.EnjoyRequest(NConstants.likes,clients: false, EnjoyError.self,parameters,isHeaders: true,.post) { (message, errorMessage, error) in
                loadingIndicator.stopAnimating()
                sender.isEnabled = true
                if error != nil {
                    self.enjoyToast(error!.localizedDescription)
                    return
                }
                
                if errorMessage.isEmpty == false {
                    self.enjoyToast(errorMessage)
                } else {
                    if message!.success == true {
                        //self.enjoyToast(message!.message)
                    } else {
                       // self.enjoyToast(message!.message)
                    }
                }
            }
        }
    }
    
    func makeNavTransparent(nav: UINavigationController) {
        nav.navigationBar.setBackgroundImage(UIImage(), for: .default)
        nav.navigationBar.shadowImage = UIImage()
        nav.navigationBar.isTranslucent = true
    }
    
    func applyEnjoyGradient(nav: UINavigationController, isTransparent: Bool = false,searchBar: UISearchBar? = nil,collectionView: UICollectionView? = nil) {
        if isTransparent {
            makeNavTransparent(nav: nav)
        } else {
            let gradientLayer = CAGradientLayer()
            let updatedFrame = nav.navigationBar.bounds
            gradientLayer.frame = updatedFrame
            gradientLayer.colors = [UIColor(hex: 0x4b0965).cgColor, UIColor(hex: 0xa7386f).cgColor]
            gradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
            gradientLayer.endPoint = CGPoint(x: 1, y: 0.5)
            
            UIGraphicsBeginImageContext(gradientLayer.bounds.size)
            gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            nav.navigationBar.isTranslucent = false
            nav.navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
            if let searchBar = searchBar {
                
                if #available(iOS 13.0, *) {
                searchBar.searchTextField.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                searchBar.searchTextField.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                }
                searchBar.setBackgroundImage(image, for: .any , barMetrics: .default)
                nav.navigationBar.shadowImage = UIImage()
            }
            
            if let collectionView = collectionView {
                let collectionViewBackgroundView = UIView()
                gradientLayer.frame.size = view.frame.size
                collectionView.backgroundView = collectionViewBackgroundView
                collectionView.backgroundView?.layer.addSublayer(gradientLayer)
                collectionView.backgroundColor = #colorLiteral(red: 0.09019608051, green: 0, blue: 0.3019607961, alpha: 1)
                nav.navigationBar.shadowImage = UIImage()
            }
        }
    }
}

