//
//  Extenstions.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/10/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import Foundation
import UIKit
import Toast_Swift
import MOLH
import NVActivityIndicatorView

@IBDesignable
class DesignableView: UIView {
}

@IBDesignable
class DesignableLabel: UILabel {
}

@IBDesignable
class DesignableStackView: UIStackView {
}

@IBDesignable
class DesignableImageView: UIImageView {
}

@IBDesignable
class DesignableSearch: UISearchBar {
    
    @IBInspectable var rightImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    func updateView() {
        if let image = rightImage {
            var xPos: CGFloat = 0
            if MOLHLanguage.currentAppleLanguage() == "en" {
                xPos = self.frame.width - 40
            } else {
                xPos = 20
            }
            //let height = self.frame.height * 0.2
            let yPos = ((self.frame.height) / 2) - 17
            let imageView = UIImageView(frame: CGRect(x: xPos, y: yPos, width: 16, height: 16))
            imageView.contentMode = .scaleToFill
            imageView.image = image
            let textFieldInsideSearchBar = self.value(forKey: "searchField") as? UITextField
            textFieldInsideSearchBar?.borderStyle = .none
            textFieldInsideSearchBar?.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textFieldInsideSearchBar?.cornerRadius = 10
            textFieldInsideSearchBar?.leftView = nil
            textFieldInsideSearchBar?.addSubview(imageView)
            // textFieldInsideSearchBar?.rightView = imageView
        }
    }
}

@IBDesignable
class DesignableUITextField: UITextField {
    
    // Provides left padding for images
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.leftViewRect(forBounds: bounds)
        if MOLHLanguage.currentAppleLanguage() == "en" {
            textRect.origin.x += Padding
        } else {
            textRect.origin.x -= Padding
        }
        return textRect
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.rightViewRect(forBounds: bounds)
        textRect.origin.x -= Padding
        return textRect
    }
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var Padding: CGFloat = 0
    
    @IBInspectable var color: UIColor = UIColor.lightGray {
        didSet {
            updateView()
        }
    }
    
    func updateView() {
        if let image = leftImage {
            leftViewMode = UITextField.ViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.contentMode = .scaleAspectFit
            imageView.image = image
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = color
            leftView = imageView
        } else {
            leftViewMode = UITextField.ViewMode.never
            leftView = nil
        }
        
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: color])
    }
    
    @IBInspectable var leftPadding: CGFloat {
        get {
            return (self.rightView?.bounds.width)!
        }
        set {
            if MOLHLanguage.currentAppleLanguage() == "en" {
                let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: self.frame.size.height))
                self.rightView = paddingView
            } else {
                let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue + 17, height:self.frame.size.height))
                self.rightView = paddingView
            }
            self.rightViewMode = .always
        }
    }}

@IBDesignable
class DesignableButton: UIButton {
    
    @IBInspectable var leftHandImage: UIImage? {
        didSet {
            leftHandImage = leftHandImage?.withRenderingMode(.alwaysOriginal)
            setupValues()
        }
    }
    
    @IBInspectable var rightHandImage: UIImage? {
        didSet {
            rightHandImage = rightHandImage?.withRenderingMode(.alwaysTemplate)
            setupValues()
        }
    }
    
    @IBInspectable var leftImageSize: CGSize = CGSize(width: 0, height: 0) {
        didSet {
            setupValues()
        }
    }
    
    @IBInspectable var rightImageSize: CGSize = CGSize(width: 0, height: 0) {
        didSet {
            setupValues()
        }
    }
    
    @IBInspectable var leftImagePadding: CGFloat = 20.0 {
        didSet {
            setupValues()
        }
    }
    
    @IBInspectable var RightImagePadding: CGFloat = 30.0 {
        didSet {
            setupValues()
        }
    }
    
    @IBInspectable var LeftImageRtl: Bool = false {
        didSet {
            setupValues()
        }
    }
    
    @IBInspectable var RightImageRtl: Bool = false {
        didSet {
            setupValues()
        }
    }
    
    func setupValues() {
        setupImages(leftImageSize.width,leftImageSize.height,rightImageSize.width,rightImageSize.height,leftImagePadding,RightImagePadding,LeftImageRtl,RightImageRtl)
    }
    
    func setupImages(_ LWidth: CGFloat = 16.7,_ LHieght: CGFloat = 16.7,_ RWidth: CGFloat = 13.3,_ RHieght: CGFloat = 13.3,_ leftPadding: CGFloat = 20.0,_ rightPadding: CGFloat = 30.0,_ leftImageRtl: Bool = false,_ rightImageRtl: Bool = false)  {
        if let leftImage = leftHandImage {
            let leftImageView = UIImageView(image: leftImage)
            var xPos: CGFloat = 0
            if MOLHLanguage.currentAppleLanguage() == "en" {
                xPos = leftPadding
            } else {
                xPos = self.frame.width - (leftPadding * 2)
                if leftImageRtl {
                    leftImageView.transform = CGAffineTransform(scaleX: -1, y: 1)
                }
            }
            let yPos = (self.frame.height - (self.frame.height / 2) - (LHieght / 2))
            leftImageView.frame = CGRect(x: xPos, y: yPos, width: LWidth, height: LHieght)
            self.addSubview(leftImageView)
        }
        
        if let rightImage = rightHandImage {
            let rightImageView = UIImageView(image: rightImage)
            rightImageView.tintColor = self.tintColor
            let height = self.frame.height * 0.2
            var xPos: CGFloat = 0
            if MOLHLanguage.currentAppleLanguage() == "en" {
                xPos = self.frame.width - rightPadding
            } else {
                xPos = rightPadding
                if rightImageRtl {
                    rightImageView.transform = CGAffineTransform(scaleX: -1, y: 1)
                }
            }
            let yPos = (self.frame.height - height) / 2
            rightImageView.frame = CGRect(x: xPos, y: yPos, width: RWidth, height: RHieght)
            self.addSubview(rightImageView)
        }
    }
}



extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable var shadowColor: UIColor? {
        get {
            return UIColor(cgColor: layer.shadowColor!)
        }
        set {
            layer.shadowColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var maskToBounds: Bool {
        get {
            return layer.masksToBounds
        }
        set {
            layer.masksToBounds = newValue
        }
    }
    
    @IBInspectable var MaskedCorners: String {
        get {
            return ""
        }
        set {
            layer.maskedCorners = []
            if newValue.contains("1") {
                layer.maskedCorners.update(with: .layerMaxXMinYCorner)
            }
            if newValue.contains("2") {
                layer.maskedCorners.update(with: .layerMinXMinYCorner)
            }
            if newValue.contains("3") {
                layer.maskedCorners.update(with: .layerMinXMaxYCorner)
            }
            if newValue.contains("4") {
                layer.maskedCorners.update(with: .layerMaxXMaxYCorner)
            }
        }
    }
}

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
    var url: URL {
        return URL(string: self)!
    }
    
    var length: Int {
        return count
    }
    
    subscript (i: Int) -> String {
        return self[i ..< i + 1]
    }
    
    func substring(fromIndex: Int) -> String {
        return self[min(fromIndex, length) ..< length]
    }
    
    func substring(toIndex: Int) -> String {
        return self[0 ..< max(0, toIndex)]
    }
    
    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
                                            upper: min(length, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return String(self[start ..< end])
    }
}

extension UIColor {
    
    convenience init(hex: Int) {
        let components = (
            R: CGFloat((hex >> 16) & 0xff) / 255,
            G: CGFloat((hex >> 08) & 0xff) / 255,
            B: CGFloat((hex >> 00) & 0xff) / 255
        )
        self.init(red: components.R, green: components.G, blue: components.B, alpha: 1)
    }
}

extension CGColor {
    
    class func colorWithHex(hex: Int) -> CGColor {
        return UIColor(hex: hex).cgColor
    }
}

public class BadgeBarButtonItem: UIBarButtonItem
{
    @IBInspectable
    public var badgeNumber: Int = 0 {
        didSet {
            self.updateBadge()
        }
    }
    
    private let label: UILabel
    
    required public init?(coder aDecoder: NSCoder)
    {
        let label = UILabel()
        label.backgroundColor = #colorLiteral(red: 0.7525976896, green: 0.301014483, blue: 0.5244979262, alpha: 1)
        label.alpha = 0.9
        label.layer.cornerRadius = 9
        label.clipsToBounds = true
        label.font = label.font.withSize(10)
        label.isUserInteractionEnabled = false
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = .white
        label.layer.zPosition = 1
        self.label = label
        
        super.init(coder: aDecoder)
        
        self.addObserver(self, forKeyPath: "view", options: [], context: nil)
    }
    
    override public func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        print("Notification Updated")
        self.updateBadge()
    }
    
    private func updateBadge()
    {
        guard let view = self.value(forKey: "view") as? UIView else { return }
        
        self.label.text = "\(EnjoyData.shared.getIntValue(Constants.notificationCount))"
        
        if self.badgeNumber > 0 && self.label.superview == nil
        {
            view.addSubview(self.label)
            
            self.label.widthAnchor.constraint(equalToConstant: 16).isActive = true
            self.label.heightAnchor.constraint(equalToConstant: 16).isActive = true
            self.label.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 9).isActive = true
            self.label.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -9).isActive = true
        }
        else if self.badgeNumber == 0 && self.label.superview != nil
        {
            self.label.removeFromSuperview()
        }
    }
    
    deinit {
        self.removeObserver(self, forKeyPath: "view")
    }
}
