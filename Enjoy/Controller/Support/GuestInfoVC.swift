//
//  GuestInfoVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/23/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit

class GuestInfoVC: UIViewController {

    @IBOutlet weak var guestName: DesignableUITextField!
    @IBOutlet weak var guestPhoneNumber: DesignableUITextField!
    @IBOutlet weak var guestEmail: DesignableUITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupField(guestName)
        setupField(guestPhoneNumber)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        makeNavTransparent(nav: self.navigationController!)
    }
    
    @IBAction func startChat(_ sender: UIButton) {
        if guestName.text!.isEmpty {
            enjoyToast("Please Enter Your Name".localized)
            return
        }
        
        if guestPhoneNumber.text!.isEmpty {
            enjoyToast("Please Enter Your Phone Number".localized)
            return
        }
        
        let chatVC = self.storyboard!.instantiateViewController(withIdentifier: "SupportChatVC") as! SupportChatVC
        chatVC.name = guestName.text!
        chatVC.phoneNumber = guestPhoneNumber.text!
        chatVC.email = guestEmail.text!
        self.navigationController?.pushViewController(chatVC, animated: true)
    }
    
    func setupField(_ textfield: DesignableUITextField) {
        let attributedString = NSMutableAttributedString(string: textfield.placeholder!)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red , range: (textfield.placeholder! as NSString).range(of: "*"))
        textfield.attributedPlaceholder = attributedString
    }
}
