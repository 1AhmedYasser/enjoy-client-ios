//
//  SupportChatVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/23/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import GrowingTextView
import LocalizedTimeAgo
import IQKeyboardManager
import Firebase
import MOLH
import Kingfisher

class SupportChatVC: UIViewController, UITextViewDelegate {
    
    var name: String?
    var phoneNumber: String?
    var email: String?
    let enjoyDBReference = Database.database().reference()
    @IBOutlet weak var message: GrowingTextView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var notificationIcon: BadgeBarButtonItem!
    
    var messages = [Message]()
    var guestMessages = [GuestMessage]()
    var userLoggedIn = (EnjoyData.shared.getStringValue(Constants.userToken) == "") ? false : true
    var notified = false
    
    var viewOriginY: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 120
        
        IQKeyboardManager.shared().isEnabled = false
        message.placeholder = " Type your message here".localized
        
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            message.textAlignment = .right
            sendButton.transform = CGAffineTransform(scaleX: -1, y: 1)
        } else {
            message.textAlignment = .left
        }
        
        loadAndObserveMessages()
        
        if EnjoyData.shared.getStringValue(Constants.userToken) == "" {
            if MOLHLanguage.currentAppleLanguage() == "ar" {
                let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "RightArrowLanguage"), style: .plain, target: self, action: #selector(goBack(_:)))
                backButton.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                self.navigationItem.leftBarButtonItem  = backButton
                
            } else {
                
                let leftBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "LeftArrow"), style: .done, target: self, action: #selector(goBack(_:)))
                leftBarButton.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                self.navigationItem.leftBarButtonItem = leftBarButton
            }
            
            
            applyEnjoyGradient(nav: self.navigationController!)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        notificationIcon.badgeNumber = EnjoyData.shared.getIntValue(Constants.notificationCount)
        
        viewOriginY = view.frame.origin.y
        IQKeyboardManager.shared().isEnabled = false
        subscribeToKeyboardNotifications()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        enjoyDBReference.removeAllObservers()
        IQKeyboardManager.shared().isEnabled = true
        unsubscribeFromKeyboardNotifications()
    }
    
    func loadAndObserveMessages() {
        if userLoggedIn {
            observePath(path: "Messages",dbPath: EnjoyData.shared.getStringValue(Constants.userId))
        } else {
            observePath(path: "MessagesNotLoged",dbPath: phoneNumber!)
        }
    }
    
    func observePath(path: String,dbPath: String) {
        enjoyDBReference.child(path).child("\(dbPath)").observe(.childAdded) { (snapshot) in
            if let messageData = snapshot.value as? [String:Any] {
                if messageData.count >= 4 {
                    if self.userLoggedIn {
                        self.messages.append(Message(data: messageData))
                    } else {
                        self.guestMessages.append(GuestMessage(data: messageData))
                    }
                    let row = (self.userLoggedIn) ? self.messages.count - 1 : self.guestMessages.count - 1
                    self.tableView.beginUpdates()
                    self.tableView.insertRows(at: [(NSIndexPath(row: row, section: 0) as IndexPath)], with: .automatic)
                    self.tableView.endUpdates()
                    self.tableView.scrollToRow(at: NSIndexPath(row: row, section: 0) as IndexPath, at: .bottom, animated: true)
                }
            }
        }
        
    }
    
    @IBAction func sendMessage(_ sender: UIButton) {
        if message!.text.isEmpty {
            print("Message is Empty")
            return
        }
        
        if userLoggedIn {
            
            let messages = enjoyDBReference.child("Messages").child("\(EnjoyData.shared.getIntValue(Constants.userId))").childByAutoId()
            let messageData: [String:Any] = ["CreatedTime":Date().timeIntervalSince1970,
                                             "Message":message!.text ?? "",
                                             "id":EnjoyData.shared.getStringValue(Constants.userId),
                                             "sender":"mobile"]
            message.text = ""
            messages.setValue(messageData)
            
            if notified == false {
                notify("Messages",EnjoyData.shared.getStringValue(Constants.userId),"Welcome to enjoy we will answer sooner".localized)
                notified = true
            }
        } else {
            let messages = enjoyDBReference.child("MessagesNotLoged").child(phoneNumber!).childByAutoId()
            let messageData: [String:Any] = ["CreatedTime":Date().timeIntervalSince1970,
                                             "Message":message!.text ?? "",
                                             "Name":name ?? "",
                                             "id":phoneNumber!,
                                             "sender":"mobile"]
            message.text = ""
            messages.setValue(messageData)
            
            if notified == false {
                notify("MessagesNotLoged",phoneNumber!,"Welcome to enjoy we will answer sooner".localized)
                notified = true
            }
        }
    }
    
    func notify(_ nodeName: String ,_ nodeChildId: String ,_ message: String) {
        print("Notified")
        let messages = enjoyDBReference.child(nodeName).child(nodeChildId).childByAutoId()
        let messageData: [String:Any] = ["CreatedTime":Date().timeIntervalSince1970,
                                         "Message":message,
                                         "id":nodeChildId,
                                         "sender":"web"]
        messages.setValue(messageData)
        let parameters = ["node_name":nodeName,"node_child_id":nodeChildId,"message":message]
        EnjoyAPI.EnjoyRequest(NConstants.notification,clients: false,EnjoyError.self,parameters,isHeaders: true,.post) { (message, errorMessage, error) in
            if error != nil {
                print(error!.localizedDescription)
                return
            }
            
            if errorMessage.isEmpty == false {
                self.enjoyToast(errorMessage)
            } else {
                if message!.success == true {
                    print(message!.message)
                } else {
                    print(message!.message)
                }
            }
        }
    }
}

extension SupportChatVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if userLoggedIn {
            return messages.count
        } else {
            return guestMessages.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let chatCell = tableView.dequeueReusableCell(withIdentifier: "ChatCell", for: indexPath) as! ChatCell
        if userLoggedIn {
            chatCell.userImage.kf.setImage(
                with: URL(string: EnjoyData.shared.getStringValue(Constants.userImage)),
                options: [
                    .scaleFactor(UIScreen.main.scale),
                    .transition(.fade(1)),
                    .cacheOriginalImage
            ])
            chatCell.message.text = self.messages[indexPath.row].Message
            chatCell.time.text = Date(timeIntervalSince1970: self.messages[indexPath.row].CreatedTime as! TimeInterval).timeAgo()
            self.messages[indexPath.row].sender == "web" ? chatCell.setupSupportMessage() : chatCell.setupUserMessage()
        } else {
            chatCell.message.text = self.guestMessages[indexPath.row].Message
            chatCell.time.text = Date(timeIntervalSince1970: self.guestMessages[indexPath.row].CreatedTime as! TimeInterval).timeAgo()
            self.guestMessages[indexPath.row].sender == "web" ? chatCell.setupSupportMessage() : chatCell.setupUserMessage()
        }
        
        return chatCell
    }
    
    
    // MARK: keyboard Settings
    // Activate keyboard notifications on keyboard presence and absence
    func subscribeToKeyboardNotifications() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    // Disables keyboard notifications
    func unsubscribeFromKeyboardNotifications() {
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    // if the bottom text field is the first responder then shift the view up
    @objc func keyboardWillShow(_ notification:Notification) {
        if message.isFirstResponder {
            view.frame.origin.y = -getKeyboardHeight(notification) + viewOriginY
        }
    }
    
    // if the keyboard is dismissed then shift the view back to its original state
    @objc func keyboardWillHide(_ notification:Notification) {
        view.frame.origin.y = viewOriginY
    }
    
    // Returns the keyboard height as a float value
    func getKeyboardHeight(_ notification:Notification) -> CGFloat {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue // of CGRect
        return keyboardSize.cgRectValue.height
    }
}
