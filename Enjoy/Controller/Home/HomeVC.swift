//
//  HomeVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/12/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import AVKit
import KafkaRefresh
import NVActivityIndicatorView

class HomeVC: UIViewController {
    
    @IBOutlet weak var notificationIcon: BadgeBarButtonItem!
    
    @IBOutlet weak var statusBtn: UIButton!
    @IBOutlet weak var videosCollectionView: UICollectionView!
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    
    @IBOutlet weak var flowlayout: UICollectionViewFlowLayout!
    var page: Int = 1
    var isLastPage = false
    var isFirstLoad = true
    
    var slider = [Slider]()
        
    var dataSource : [AnyObject] = [];
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        videosCollectionView.register(UINib(nibName: "VideoCell", bundle: .main),forCellWithReuseIdentifier: "VideoCell")
        videosCollectionView.bindFootRefreshHandler({
            if self.isLastPage == false {
                self.page += 1
                self.loadVideos(isLoadingMore: true, page: self.page)
            }
        }, themeColor: #colorLiteral(red: 0.3294117647, green: 0.1588959098, blue: 0.5214938521, alpha: 1), refreshStyle: .replicatorCircle)
        videosCollectionView.footRefreshControl.autoRefreshOnFoot = true
        
        loadVideos()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notificationIcon.badgeNumber = EnjoyData.shared.getIntValue(Constants.notificationCount)
        self.navigationController?.isNavigationBarHidden = false
        makeNavTransparent(nav: self.navigationController!)
    }
    
    fileprivate func loadVideos(isLoadingMore: Bool = false ,page: Int = 1) {
        (isLoadingMore == false) ? loadingIndicator.startAnimating() : nil
        let parameters = ["page":page,"client_id":EnjoyData.shared.getIntValue(Constants.userId)]
        EnjoyAPI.EnjoyRequest(NConstants.home,clients: false, Home.self,parameters,isHeaders: true) { (videos, errorMessage, error) in
            (isLoadingMore == false) ? self.loadingIndicator.stopAnimating() : nil
            self.videosCollectionView.footRefreshControl.endRefreshing()
            if error != nil {
                self.enjoyToast(error!.localizedDescription)
                return
            }
            
            if errorMessage.isEmpty == false {
                self.enjoyToast(errorMessage)
            } else {
                if videos!.success == true {
                    if videos!.data.videos.videos.count == 0 {
                        self.videosCollectionView.footRefreshControl = nil
                        self.isLastPage = true
                    } else {
                        _ = self.dataSource.count
                        for video in videos!.data.videos.videos {
                            self.dataSource.append(video as AnyObject)
                        }
                        self.dataSource.append(videos!.data.videos.ad as AnyObject)
                        self.slider = videos!.data.slider
                        
//                                                UIView.transition(with: self.videosCollectionView, duration: 0.5, options: .transitionCrossDissolve, animations: {
//                                                    self.videosCollectionView.insertItems(at: [IndexPath(row: index, section: 0)])
//                                                }, completion: nil)
                        
                        if self.isFirstLoad {
                            UIView.transition(with: self.videosCollectionView, duration: 0.5, options: .transitionCrossDissolve, animations: {
                                self.videosCollectionView?.reloadData()
                            })
                            self.isFirstLoad = false
                        } else {
                            self.videosCollectionView?.reloadData()
                        }
                    }
                } else {
                    self.enjoyToast(videos!.message)
                }
            }
        }
    }
}

extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if self.dataSource[indexPath.row] is Ad {
            let adCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AdCell", for: indexPath) as! AdCell
            adCell.setupAd(image: (self.dataSource[indexPath.row] as! Ad).image)
            return adCell
        } else {
            let videoCell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoCell", for: indexPath) as! VideoCell
            videoCell.setupVideo(video: self.dataSource[indexPath.row] as! VideosData)
            videoCell.delegate = self
            
            return videoCell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerCell = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HomeHeader", for: indexPath) as! HomeHeader
        headerCell.sliderVideos = slider
        headerCell.sliderControl.numberOfPages = slider.count
        headerCell.promosCollectionView.reloadData()
        return headerCell
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width, height: 250)
    }
}

extension HomeVC: VideoCellDelegate {
    func fullScreenPressed(cell: VideoCell) {
        if cell.player != nil {
            let playerViewController = AVPlayerViewController()
            playerViewController.player = cell.player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
    }
    
    func openMediaDetails(cell: VideoCell) {
        let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "MediaDetailsVC") as! MediaDetailsVC
        vc.video = cell.video
        vc.artistImg = cell.artistImage
        vc.videoThumb = cell.thumbnail
        vc.cell = cell
        makeNavTransparent(nav: self.navigationController!)
        vc.callback = { (likes,isLiked) in
            cell.videoLikes.text = "\(likes)"
            (isLiked) ? (cell.likesImage.image = #imageLiteral(resourceName: "thumbs_up_filled")) : (cell.likesImage.image = #imageLiteral(resourceName: "thumbsUp"))
        }
        self.navigationController!.pushViewController(vc, animated: true)
    }
    
    func likeVideo(cell: VideoCell) {
        like(cell: cell,sender: cell.likesButton, likesLabel: cell.videoLikes, likesImage: UIImageView(), videoId: cell.video!.id, loadingIndicator: self.loadingIndicator,isButton: true)
    }
}
