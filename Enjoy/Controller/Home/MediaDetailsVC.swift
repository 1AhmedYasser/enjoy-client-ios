//
//  MediaDetailsVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/21/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import AVKit
import Photos
import MOLH
import MBProgressHUD
import NVActivityIndicatorView

class MediaDetailsVC: UIViewController {
    
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var artistImage: UIImageView!
    @IBOutlet weak var videoTitle: UILabel!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var views: UILabel!
    @IBOutlet weak var likes: UILabel!
    @IBOutlet weak var likesImage: UIImageView!
    @IBOutlet weak var videoDescription: UILabel!
    @IBOutlet weak var videoDuration: UILabel!
    @IBOutlet weak var videoCost: UILabel!
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    
    var video: VideosData?
    var artistImg: UIImageView?
    var videoThumb: UIImageView?
    var player: AVPlayer?
    var cell: VideoCell?
    
    // Video Download Properties
    var progress: Float = 0.0
    var task: URLSessionTask?
    
    var callback : ((Int,Bool)->())?
    
    lazy var session : URLSession = {
        return URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: OperationQueue.main)
    }()
    
    let controlsView: UIView = {
        let controls = UIView()
        controls.backgroundColor = UIColor(white: 0, alpha: 0)
        return controls
    }()
    
    let pauseButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "play"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        return button
    }()
    
    let thumbnail: UIImageView = {
        let thumbnail = UIImageView()
        thumbnail.contentMode = .scaleToFill
        thumbnail.translatesAutoresizingMaskIntoConstraints = false
        return thumbnail
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDetails()
        pauseButton.addTarget(self, action: #selector(changeStatus), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        makeNavTransparent(nav: self.navigationController!)
        
        
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "RightArrowLanguage"), style: .plain, target: self, action: #selector(goBack(_:)))
            backButton.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.navigationItem.leftBarButtonItem  = backButton
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.task?.cancel()
    }
    
    func setupDetails() {
        artistImage.image = artistImg?.image
        videoTitle.text = video?.title
        artistName.text = video?.artist.name
        views.text = "\(video?.views ?? 0)"
        likes.text = cell?.videoLikes.text
        videoDescription.text = video?.description
        if video?.video != nil {
            let asset = AVURLAsset(url: URL(string: video?.video ?? "")!)
            let duration = asset.duration.seconds
            if MOLHLanguage.currentAppleLanguage() == "en" {
                videoDuration.text = ": \(Int(duration)) Sec Maximum"
            } else {
                videoDuration.text = ": حد اقصي \(Int(duration))"
            }
        }
        if MOLHLanguage.currentAppleLanguage() == "en" {
            videoCost.text = ": \(video?.artist.price ?? 0) $"
        } else {
            videoCost.text = "$ \(video?.artist.price ?? 0) :"
        }
        thumbnail.image = videoThumb?.image
        
        if cell?.isLiked == true {
            likesImage.image = #imageLiteral(resourceName: "thumbs_up_filled")
        }
        
        if video?.video?.isEmpty == false {
            let videoURL = URL(string: video?.video! ?? "")
            player = AVPlayer(url: videoURL!)
            let playerLayer = AVPlayerLayer(player: player)
            playerLayer.frame = self.videoView.frame
            playerLayer.videoGravity = AVLayerVideoGravity.resize
            self.videoView.layer.addSublayer(playerLayer)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
        
        controlsView.frame = self.videoView.frame
        self.videoView.addSubview(controlsView)
        controlsView.addSubview(thumbnail)
        controlsView.addSubview(pauseButton)
        thumbnail.centerXAnchor.constraint(equalTo: videoView.centerXAnchor).isActive = true
        thumbnail.centerYAnchor.constraint(equalTo: videoView.centerYAnchor).isActive = true
        thumbnail.widthAnchor.constraint(equalToConstant: videoView.bounds.width).isActive = true
        thumbnail.heightAnchor.constraint(equalToConstant: videoView.bounds.height).isActive = true
        pauseButton.centerXAnchor.constraint(equalTo: videoView.centerXAnchor).isActive = true
        pauseButton.centerYAnchor.constraint(equalTo: videoView.centerYAnchor).isActive = true
        pauseButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        pauseButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    @objc func playerDidFinishPlaying() {
        pauseButton.setImage(#imageLiteral(resourceName: "replay"), for: .normal)
    }
    
    @objc func changeStatus() {
        if let player = self.player {
            thumbnail.image = nil
            if pauseButton.imageView?.image == UIImage(named: "replay") {
                pauseButton.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
                self.player?.seek(to: CMTime.zero)
                self.player?.play()
            } else {
                if player.timeControlStatus == AVPlayer.TimeControlStatus.playing {
                    pauseButton.setImage(#imageLiteral(resourceName: "play"), for: .normal)
                    player.pause()
                } else {
                    pauseButton.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
                    player.play()
                }
            }
        }
    }
    
    @IBAction func downloadVideo(_ sender: UIButton) {
        print("Download Video")
        downLoadEnjoyVideo(url:video?.video ?? "")
    }
    
    func downLoadEnjoyVideo(url:String) {
        let hud =  MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.mode = .annularDeterminate
        hud.label.text = "Downloading"
        hud.detailsLabel.text = "Cancel"
        let tap = UITapGestureRecognizer(target: self, action: #selector(cancelVideoDownload))
        hud.addGestureRecognizer(tap)
        DispatchQueue.global(qos: .default).async(execute: {() -> Void in
            while self.progress < 1.0 {
                DispatchQueue.main.async(execute: {() -> Void in
                    MBProgressHUD(for: self.view)?.progress = self.progress
                })
                usleep(50000)
            }
        })
        
        let videoRequest = URLRequest(url: url.url)
        let task = self.session.downloadTask(with: videoRequest)
        self.task = task
        task.resume()
    }
    
    @IBAction func goBackToHome(_ sender: UIBarButtonItem) {
        callback!(Int(likes.text!)!,cell!.isLiked)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func cancelVideoDownload() {
        task?.cancel()
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    @IBAction func shareVideo(_ sender: UIButton) {
        share(url: video?.video ?? "")
    }
    
    @IBAction func likeVideo(_ sender: UIButton) {
        like(cell: cell ,sender: sender, likesLabel: likes, likesImage: likesImage, videoId: video!.id, loadingIndicator: self.loadingIndicator)
    }
    
    @IBAction func RequestOrder(_ sender: UIButton) {
        print("Request Order")
        let vc = UIStoryboard(name: "Order", bundle: nil).instantiateViewController(withIdentifier: "RequestServiceVC") as! RequestServiceVC
        vc.mediaArtistDetails = video?.artist
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension MediaDetailsVC: URLSessionDownloadDelegate {
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        let x = String(format:"%.2f", Float(totalBytesWritten) / Float(totalBytesExpectedToWrite))
        self.progress = Float(x)!
        print(progress)
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        let fileManager = FileManager()
        let directoryURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        print(directoryURL)
        let docDirectoryURL = NSURL(fileURLWithPath: "\(directoryURL)")
        let destinationFilename = downloadTask.originalRequest?.url?.lastPathComponent
        let destinationURL =  docDirectoryURL.appendingPathComponent("\(destinationFilename!)")
        
        if let path = destinationURL?.path {
            if fileManager.fileExists(atPath: path) {
                do {
                    try fileManager.removeItem(at: destinationURL!)
                    
                } catch let error as NSError {
                    print(error.debugDescription)
                }
                
            }
        }
        
        do
        {
            try fileManager.copyItem(at: location, to: destinationURL!)
        }
        catch {
            print("Error while copy file")
            
        }
        DispatchQueue.main.async(execute: {() -> Void in
            MBProgressHUD.hide(for: self.view, animated: true)
        })
        let objectsToShare = [destinationURL!]
        let activityVC = UIActivityViewController(activityItems: objectsToShare , applicationActivities: nil)
        activityVC.setValue("Video", forKey: "subject")
        
        activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.assignToContact, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.mail,UIActivity.ActivityType.message, UIActivity.ActivityType.openInIBooks, UIActivity.ActivityType.postToTencentWeibo, UIActivity.ActivityType.postToVimeo, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.print]
        self.present(activityVC, animated: true, completion: nil)
        
    }
}
