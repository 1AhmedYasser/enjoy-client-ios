//
//  OurWorksVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/18/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import AVKit
import NVActivityIndicatorView

class OurWorksVC: UIViewController {
    
    @IBOutlet weak var notificationIcon: BadgeBarButtonItem!
    @IBOutlet weak var categoriesCV: UICollectionView!
    @IBOutlet weak var videosCV: UICollectionView!
    
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    
    @IBOutlet weak var flowlayout: UICollectionViewFlowLayout!
    
    var videos = [VideosData]()
    var categories = [Category]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        videosCV.register(UINib(nibName: "VideoCell", bundle: .main),forCellWithReuseIdentifier: "VideoCell")
        
        flowlayout.itemSize = UICollectionViewFlowLayout.automaticSize
        flowlayout.estimatedItemSize = CGSize(width: 55, height: 35)
        
        loadCategories()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notificationIcon.badgeNumber = EnjoyData.shared.getIntValue(Constants.notificationCount)
        if self.navigationController!.navigationBar.isTranslucent {
            applyEnjoyGradient(nav: self.navigationController!)
        }
    }
    
    fileprivate func loadCategories() {
        loadingIndicator.startAnimating()
        EnjoyAPI.EnjoyRequest(NConstants.categories,clients: false, Categories.self,isHeaders: false) { (categories, errorMessage, error) in
            if error != nil {
                self.loadingIndicator.stopAnimating()
                self.enjoyToast(error!.localizedDescription)
                return
            }
            
            if errorMessage.isEmpty == false {
                self.loadingIndicator.stopAnimating()
                self.enjoyToast(errorMessage)
            } else {
                if categories!.success == true {
                    self.categories = categories!.data
                    print(self.categories)
                    self.loadVideos(categoryId: self.categories[0].id)
                } else {
                    self.enjoyToast(categories!.message)
                }
            }
        }
    }
    
    fileprivate func loadVideos(categoryId: Int,switchingCat: Bool = false) {
        self.videos.removeAll()
        self.videosCV.reloadData()
        loadingIndicator.isAnimating ? nil : loadingIndicator.startAnimating()
        EnjoyAPI.EnjoyRequest("\(NConstants.category)/\(categoryId)",clients: false, CategoryVideos.self,isHeaders: false) { (category, errorMessage, error) in
            self.loadingIndicator.stopAnimating()
            if error != nil {
                self.enjoyToast(error!.localizedDescription)
                return
            }
            
            if errorMessage.isEmpty == false {
                self.enjoyToast(errorMessage)
            } else {
                if category!.success == true {
                    self.videos = category!.data.videos
                    if switchingCat == false {
                        self.categoriesCV.reloadData()
                        self.categoriesCV.selectItem(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .top)
                    }
                    UIView.transition(with: self.videosCV, duration: 0.5, options: .transitionCrossDissolve, animations: {
                        self.videosCV.reloadData()
                    }, completion: nil)
                    self.videosCV.reloadData()
                } else {
                    self.enjoyToast(category!.message)
                }
            }
        }
    }
}

extension OurWorksVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == videosCV {
            return videos.count
        } else {
            if (categories.count != 0) {
                return categories.count
            } else {
                return 0
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == videosCV {
            let videoCell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoCell", for: indexPath) as! VideoCell
            videoCell.setupVideo(video: videos[indexPath.row])
            videoCell.delegate = self
            return videoCell
        } else {
            let categoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
            
            categoryCell.setup(categories[indexPath.row].name)
            return categoryCell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == categoriesCV {
            loadVideos(categoryId: categories[indexPath.row].id,switchingCat: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == videosCV {
            return CGSize(width: collectionView.bounds.size.width, height: 250)
        } else {
            return CGSize(width: 55, height: 35)
        }
    }
}

extension OurWorksVC: VideoCellDelegate {
    func openMediaDetails(cell: VideoCell) {
        let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "MediaDetailsVC") as! MediaDetailsVC
        vc.video = cell.video
        vc.artistImg = cell.artistImage
        vc.videoThumb = cell.thumbnail
        vc.cell = cell
        makeNavTransparent(nav: self.navigationController!)
        vc.callback = { (likes,isLiked) in
            cell.videoLikes.text = "\(likes)"
            (isLiked) ? (cell.likesImage.image = #imageLiteral(resourceName: "thumbs_up_filled")) : (cell.likesImage.image = #imageLiteral(resourceName: "thumbsUp"))
        }
        self.navigationController!.pushViewController(vc, animated: true)
    }
    
    func fullScreenPressed(cell: VideoCell) {
        if cell.player != nil {
            let playerViewController = AVPlayerViewController()
            playerViewController.player = cell.player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
    }
    
    func likeVideo(cell: VideoCell) {
        like(cell: cell, sender: cell.likesButton, likesLabel: cell.videoLikes, likesImage: UIImageView(), videoId: cell.video!.id, loadingIndicator: self.loadingIndicator,isButton: true)
    }
}
