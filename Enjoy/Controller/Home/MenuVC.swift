//
//  MenuVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/15/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import MOLH
import NVActivityIndicatorView

class MenuVC: UIViewController {
    
    @IBOutlet weak var userImage: DesignableImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadUserInfo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadUserInfo()
    }
    
    func loadUserInfo() {
        if EnjoyData.shared.getStringValue(Constants.userToken) == "" {
            logoutButton.isHidden = true
            userName.text = "Guest".localized
            userEmail.text = ""
            userImage.image = #imageLiteral(resourceName: "Guest")
        } else {
            logoutButton.isHidden = false
            userName.text = EnjoyData.shared.getStringValue(Constants.userName)
            userEmail.text = EnjoyData.shared.getStringValue(Constants.userEmail)
            userImage.kf.setImage(
                with: URL(string: EnjoyData.shared.getStringValue(Constants.userImage)),
                placeholder: userImage.image,
                options: [
                    .scaleFactor(UIScreen.main.scale),
                    .transition(.fade(1)),
                    .cacheOriginalImage
            ])
        }
    }
    
    @IBAction func openAScreen(_ sender: DesignableButton) {
        
        switch sender.tag {
        case 0:
            openMenuChoice("Home","HomeVC",true,false)
        case 1:
            openMenuChoice("Order", "MyOrdersVC")
        case 2:
            openMenuChoice("Menu","ArtistsVC",false,false)
        case 3:
            openMenuChoice("Home", "OurWorksVC",false,false)
        case 4:
            openMenuChoice("Order", "RequestServiceVC")
        case 5:
            openMenuChoice("Menu","ContactUsVC",true,false)
        case 6:
            openMenuChoice("Menu", "SettingsVC", false, true)
        case 7:
            openMenuChoice("Support","SupportChatVC",false,true,true)
        case 8:
            openMenuChoice("Menu","EnjoyDetails",false,false)
        case 9:
            share(url: EnjoyData.shared.getStringValue(Constants.shareLink))
        default:
            print("Error")
        }
    }
    
    func openMenuChoice(_ storyboard: String ,_ identifier: String,_ isTransparent : Bool = false,_ checkForLogin: Bool = true,_ isChat:Bool = false) {
        var vc = UIStoryboard(name: storyboard, bundle: nil).instantiateViewController(withIdentifier: identifier)
        if checkForLogin == true {
            if EnjoyData.shared.getStringValue(Constants.userToken) == "" {
                if isChat {
                vc = UIStoryboard(name: "Support", bundle: nil).instantiateViewController(withIdentifier: "GuestInfoVC")
                } else {
                vc = UIStoryboard(name: "Common", bundle: nil).instantiateViewController(withIdentifier: "LoginRequestVC")
                }
            }
        }
        let contentVC = UINavigationController(rootViewController: vc)
        contentVC.navigationBar.barStyle = .black
        applyEnjoyGradient(nav: contentVC, isTransparent: isTransparent)
        self.sideMenuViewController!.setContentViewController(contentVC, animated: true)
        self.sideMenuViewController!.hideMenuViewController()
    }
    
    @IBAction func logout(_ sender: UIButton) {
        logout(sender:sender,loadingIndicator: loadingIndicator)
    }
}
