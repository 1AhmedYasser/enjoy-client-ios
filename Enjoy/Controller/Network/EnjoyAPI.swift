//
//  EnjoyAPI.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/10/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import MOLH

class EnjoyAPI {
    
    class func EnjoyRequest<ResponseType: Decodable>(_ endpoint: String,clients: Bool = true,_ responseType: ResponseType.Type,_ params: Parameters = [:],isHeaders: Bool = true,_ method: HTTPMethod = .get, completionHandler: @escaping (ResponseType?,String,Error?) -> Void) {
        
        getAlamoRequest(url: NConstants.endpoint(endpoint,clients), params,isHeaders: isHeaders,method: method, responseType: ResponseType.self) { (response, errorMessage, error) in
            if let response = response {
                completionHandler(response,errorMessage,nil)
            } else {
                completionHandler(nil,errorMessage,error)
            }
        }
    }
    
    class func getAlamoRequest<ResponseType: Decodable>(url: URL,_ params: Parameters,isHeaders: Bool = true,method: HTTPMethod = .get, responseType: ResponseType.Type, completion: @escaping (ResponseType?,String, Error?) -> Void) {

        var headers: HTTPHeaders
        if isHeaders == true {
         headers = [
            "Authorization": "\(EnjoyData.shared.getStringValue(Constants.userToken))",
            "Accept": "application/json",
            "Accept-Language": MOLHLanguage.currentAppleLanguage()]
        } else {
            headers = [ "Accept": "application/json",
                        "Accept-Language": MOLHLanguage.currentAppleLanguage()]
        }
        
        let manager = Alamofire.SessionManager(configuration: .default)
    
           manager.request(url,method: method ,parameters: params, headers: headers).validate().responseJSON { response in
             print("url \(url)")
            //print(String(data:response.data!,encoding:.utf8)!)
            let decoder = JSONDecoder()
            switch response.result {
            case .success:
                do {
                    let responseObject = try decoder.decode(ResponseType.self, from: response.data!)
                    DispatchQueue.main.async {
                        completion(responseObject,"",nil)
                    }
                } catch {
                    print(error)
                    do {
                        let responseObject = try decoder.decode(EnjoyError.self, from: response.data!)
                        DispatchQueue.main.async {
                            completion(nil,responseObject.message,nil)
                        }
                    } catch {
                        print(error)
                    }
                    
                }
            case .failure(let error):
                completion(nil,"",error)
            }
            manager.session.invalidateAndCancel()
        }
    }
}
